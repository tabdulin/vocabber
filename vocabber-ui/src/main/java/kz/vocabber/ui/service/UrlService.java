package kz.vocabber.ui.service;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author tabdulin
 */
@Service
public class UrlService {
    private static final Logger logger = LoggerFactory.getLogger(UrlService.class);

    public String redirect(Url url) {
        try {
            logger.trace("Redirecting to: {}", url);
            FacesContext.getCurrentInstance().getExternalContext().redirect(
                    getRoot() + url.getUrl());
        } catch (IOException e) {
            logger.error("Cannot redirect", e);
        }

        return getRoot() + url.getUrl();
    }

    public String getContextPath() {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
    }

    public String getRoot() {
        return getContextPath() + "/ui/";
    }

    public Url getCurrent() {
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        for (Url url : Url.values()) {
            if (StringUtils.endsWith(request.getRequestURI(), url.getUrl())) {
                return url;
            }
        }

        return null;
    }

    public void reload() {
        redirect(getCurrent());
    }

    public String getParameter(String parameter) {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(parameter);
    }
}
