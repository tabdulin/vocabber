package kz.vocabber.ui.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.faces.context.FacesContext;
import java.util.Map;

/**
 * @author Talgat_Abdulin
 */
@Service
public class SessionMapService {
    private static final Logger logger = LoggerFactory.getLogger(SessionMapService.class);
    
    public static final String FILTERS_PREFIX = "filters_";

    public void add(String name, Object data) {
        logger.info("Adding data to session: {} - {}", name, data);
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap()
                .put(name, data);
    }
    
    public Object getAndRemove(String name) {
        Map<String,Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        Object data = sessionMap.get(name);
        logger.info("Retrieving data from session: {} - {}", name, data);
        if (sessionMap.containsKey(name)) {
            sessionMap.remove(name);
            logger.info("Removing data from session: {}", !sessionMap.containsKey(name));
        }

        return data;
    }
    
    public Object get(String name) {
        Map<String,Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
        Object data = sessionMap.get(name);
        logger.info("Retrieving data from session: {} - {}", name, data);
        return data;
    }
}
