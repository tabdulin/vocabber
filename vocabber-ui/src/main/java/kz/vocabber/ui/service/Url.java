package kz.vocabber.ui.service;

/**
 * @author tabdulin
 */
public enum Url {
    FILE("upload/file.jsf"),
    TEXT("upload/text.jsf"),
    LINK("upload/link.jsf"),
    ABOUT("about.jsf"),
    FILTER("filter.jsf"),
    UPLOADS("upload/history.jsf");

    private String url;

    private Url(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
