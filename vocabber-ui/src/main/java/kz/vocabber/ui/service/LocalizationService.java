package kz.vocabber.ui.service;

import kz.vocabber.api.exception.VocabberException;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * @author Talgat_Abdulin
 */
@Component
public class LocalizationService {
    @Resource(name = "messages")
    private MessageSource i18n;

    public String error(VocabberException e) {
        return error(e.getKey(), e.getParams());
    }
    
    public String error(String code, Object... params) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                i18n.getMessage(code, params, null),
                null);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        return null;
    }

    public String info(String code, Object... params) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
                i18n.getMessage(code, params, null),
                null);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        return null;
    }
}
