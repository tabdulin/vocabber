package kz.vocabber.ui;

import kz.vocabber.api.UserService;
import kz.vocabber.api.VocabberService;
import kz.vocabber.api.dto.WordTranslationDTO;
import kz.vocabber.api.exception.VocabberException;
import kz.vocabber.db.model.user.UserWord;
import kz.vocabber.impl.users.UserRole;
import kz.vocabber.ui.service.LocalizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author tabdulin
 */
@Component
public class MarkerBean {
    private static final Logger logger = LoggerFactory.getLogger(MarkerBean.class);

    @Resource
    private LocalizationService localizationService;
    @Resource
    private VocabberService vocabberService;
    @Resource
    private UserService userService;

    public boolean canMark() {
        return userService.getCurrentUser().getRole() == UserRole.ROLE_USER;
    }

    public boolean canRemember(UserWord.Status status) {
        return canMark() && status != UserWord.Status.learnt;
    }

    public boolean canForget(UserWord.Status status) {
        return canMark() && status == UserWord.Status.learnt;
    }

    public String forget(WordTranslationDTO word) {
        try {
            vocabberService.forget(word);
            return localizationService.info("info.word.forgot", word.getWord());
        } catch (VocabberException e) {
            return localizationService.error(e);
        }
    }

    public String remember(WordTranslationDTO words) {
        try {
            vocabberService.remember(words);
            return localizationService.info("info.word.remembered", words.getWord());
        } catch (VocabberException e) {
            return localizationService.error(e);
        }
    }

    public String rememberPage(List<WordTranslationDTO> words) {
        try {
            vocabberService.remember(words.toArray(new WordTranslationDTO[words.size()]));
            return localizationService.info("info.word.page.remembered");
        } catch (VocabberException e) {
            return localizationService.error(e);
        }
    }
}
