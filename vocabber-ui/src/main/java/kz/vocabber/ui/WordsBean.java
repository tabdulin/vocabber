package kz.vocabber.ui;

import kz.vocabber.api.VocabberService;
import kz.vocabber.api.dto.TextDTO;
import kz.vocabber.api.dto.WordDTO;
import kz.vocabber.api.exception.VocabberException;
import kz.vocabber.api.word.WordFilterType;
import kz.vocabber.ui.filter.WordFilterTypeListConverter;
import kz.vocabber.ui.service.LocalizationService;
import kz.vocabber.ui.service.SessionMapService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Talgat_Abdulin
 */
@Component
@Scope("view")
public class WordsBean {
    private TextHashId hashId;
    private WordsDataTableModel words;

    @Resource
    private VocabberService vocabberService;
    @Resource
    private LocalizationService localizationService;
    @Resource
    private SessionMapService sessionMapService;
    @Resource
    private MarkerBean markerBean;
    @Resource
    private FilterFormBean filterForm;
    @Resource
    private WordFilterTypeListConverter wordFilterTypeListConverter;
    @Resource
    private TextHashIdConverter textHashIdConverter;

    private void init() {
        try {
            List<WordFilterType> selectedFilters = (List<WordFilterType>) sessionMapService
                    .getAndRemove(SessionMapService.FILTERS_PREFIX + hashId);
            if (selectedFilters != null) {
                filterForm.setSelectedFilters(selectedFilters);
            }

            TextDTO text = vocabberService.getText(hashId.getId(), hashId.getHash());
            List<WordDTO> filteredWords = vocabberService.filter(text.getWords(), filterForm.getSelectedFilters());
            words = new WordsDataTableModel(vocabberService, markerBean.canMark(),
                    text, filteredWords, false);
        } catch (VocabberException e) {
            localizationService.error(e);
        }
    }

    public TextHashId getHashId() {
        return hashId;
    }

    public void setHashId(TextHashId hashId) {
        this.hashId = hashId;

        if (words == null && hashId != null) {
            init();
        }
    }
    
    public WordsDataTableModel getWords() {
        return words;
    }

    public void filter() {
        try {
            List<WordDTO> filteredWords = vocabberService.filter(words.getText().getWords(),
                    filterForm.getSelectedFilters());
            words.setWords(filteredWords);
        } catch (VocabberException e) {
            localizationService.error(e);
        }
    }
    
    public String print() {
        return "print.jsf?faces-redirect=true&id=" + textHashIdConverter.toString(hashId) + "&filters="
                + wordFilterTypeListConverter.toString(filterForm.getSelectedFilters());
    }
}
