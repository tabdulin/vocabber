package kz.vocabber.ui;

import kz.vocabber.api.VocabberService;
import kz.vocabber.api.comparator.TextDateComparator;
import kz.vocabber.api.dto.TextDTO;
import kz.vocabber.api.exception.VocabberException;
import kz.vocabber.ui.service.LocalizationService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

/**
 * @author Talgat_Abdulin
 */
@Component
@Scope("view")
public class TextHistoryBean {
    @Resource
    private VocabberService vocabberService;
    @Resource
    private LocalizationService localizationService;
    @Resource
    private TextHashIdConverter textHashIdConverter;

    private List<TextDTO> texts;

    @PostConstruct
    public void init() {
        try {
            texts = vocabberService.getHistory();

            Collections.sort(texts, new TextDateComparator(false));
        } catch (VocabberException e) {
            localizationService.error(e);
        }
    }

    public String load(TextDTO text) {
        return "/ui/words.jsf?faces-redirect=true&id=" +
                textHashIdConverter.toString(text.getId(), text.getHash());
    }
    
    public List<TextDTO> getTexts() {
        return texts;
    }

    public void setTexts(List<TextDTO> texts) {
        this.texts = texts;
    }
}
