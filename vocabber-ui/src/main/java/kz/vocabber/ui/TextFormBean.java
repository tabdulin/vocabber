package kz.vocabber.ui;

import kz.vocabber.api.file.FileType;
import kz.vocabber.api.file.FileTypeService;
import org.primefaces.model.UploadedFile;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;

/**
 * @author tabdulin
 */
@Component
@Scope("view")
public class TextFormBean {
    @Resource
    private FileTypeService fileTypeService;

    private UploadedFile file;
    private String text;
    private String link;

    @PostConstruct
    public void reset() {
        file = null;
        text = null;
        link = null;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public List<FileType> getFileTypes() {
        return fileTypeService.getUploadable();
    }
}
