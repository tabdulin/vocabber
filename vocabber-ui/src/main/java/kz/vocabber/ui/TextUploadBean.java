package kz.vocabber.ui;

import kz.vocabber.api.UserService;
import kz.vocabber.api.VocabberService;
import kz.vocabber.api.dto.FileDTO;
import kz.vocabber.api.dto.TextDTO;
import kz.vocabber.api.exception.VocabberException;
import kz.vocabber.ui.menu.MenuBean;
import kz.vocabber.ui.menu.MenuItem;
import kz.vocabber.ui.service.LocalizationService;
import kz.vocabber.ui.service.SessionMapService;
import kz.vocabber.ui.service.UrlService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * @author tabdulin
 */
@Component
@Scope("view")
public class TextUploadBean {
    private static final Logger logger = LoggerFactory.getLogger(TextUploadBean.class);

    @Resource
    private TextFormBean textForm;
    @Resource
    private FilterFormBean filterForm;
    @Resource
    private MarkerBean markerBean;

    @Resource
    private MenuBean menuBean;
    @Resource
    private UrlService urlService;
    @Resource
    private LocalizationService localizationService;
    @Resource
    private SessionMapService sessionMapService;
    @Resource
    private VocabberService vocabberService;
    @Resource
    private UserService userService;
    @Resource
    private TextHashIdConverter textHashIdConverter;

    public String upload() {
        TextDTO text = null;
        if (menuBean.getActive() == MenuItem.FILE) {
            try {
                FileDTO fileDTO = new FileDTO();
                fileDTO.setSize(textForm.getFile().getSize());
                fileDTO.setName(textForm.getFile().getFileName());
                fileDTO.setMime(textForm.getFile().getContentType());
                fileDTO.setStream(textForm.getFile().getInputstream());
                text = vocabberService.parseFile(fileDTO);
            } catch (IOException e) {
                return localizationService.error("error.file.upload", textForm.getFile().getFileName());
            } catch (VocabberException e) {
                return localizationService.error(e);
            }
        }

        if (menuBean.getActive() == MenuItem.TEXT) {
            try {
                text = vocabberService.parseText(textForm.getText());
            } catch (VocabberException e) {
                return localizationService.error(e);
            }
        }

        if (menuBean.getActive() == MenuItem.LINK) {
            try {
                text = vocabberService.parseWebPage(textForm.getLink());
            } catch (VocabberException e) {
                return localizationService.error(e);
            }
        }

        sessionMapService.add(SessionMapService.FILTERS_PREFIX + text.getId(), filterForm.getSelectedFilters());
        return "/ui/words.jsf?faces-redirect=true&id=" + textHashIdConverter.toString(text.getId(), text.getHash());
    }

    public String reset() {
        textForm.reset();
        filterForm.reset();
        return "?faces-redirect=true";
    }
}
