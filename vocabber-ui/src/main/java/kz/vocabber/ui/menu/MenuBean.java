package kz.vocabber.ui.menu;

import kz.vocabber.api.UserService;
import kz.vocabber.impl.users.UserRole;
import kz.vocabber.ui.service.Url;
import kz.vocabber.ui.service.UrlService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * @author tabdulin
 */
// TODO: refactor - remove
@Component
@Scope("view")
public class MenuBean {
    private static final Logger logger = LoggerFactory.getLogger(MenuBean.class);

    @Resource
    private UrlService urlService;
    @Resource
    private UserService userService;
    
    public boolean renderUploads() {
        return userService.getCurrentUser().getRole() == UserRole.ROLE_USER;
    }

    public List<MenuItem> getMenuItems() {
        return Arrays.asList(MenuItem.values());
    }

    public MenuItem getActive() {
        Url current = urlService.getCurrent();
        for (MenuItem item : MenuItem.values()) {
            if (item.getUrl() == current) {
                return item;
            }
        }

        return null;
    }
}
