package kz.vocabber.ui.menu;

import kz.vocabber.ui.service.Url;

/**
 * @author tabdulin
 */
public enum MenuItem {
    FILE("menu.file", Url.FILE),
    TEXT("menu.text", Url.TEXT),
    LINK("menu.link", Url.LINK),

    ABOUT("menu.about", Url.ABOUT),
    UPLOADS("menu.uploads", Url.UPLOADS);

    private String code;
    private Url url;

    private MenuItem(String code, Url url) {
        this.code = code;
        this.url = url;
    }

    public String getCode() {
        return code;
    }

    public Url getUrl() {
        return url;
    }
}
