package kz.vocabber.ui.filter;

import kz.vocabber.api.word.WordFilterType;

/**
 * @author tabdulin
 */
public class WordFilterTypeItem {
    private WordFilterType filter;
    private boolean selected;

    public WordFilterTypeItem(WordFilterType filter, boolean selected) {
        this.filter = filter;
        this.selected = selected;
    }

    public WordFilterType getFilter() {
        return filter;
    }

    public void setFilter(WordFilterType filter) {
        this.filter = filter;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
