package kz.vocabber.ui.filter;

import kz.vocabber.api.word.WordFilterType;
import kz.vocabber.api.word.WordFilterTypeService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Talgat_Abdulin
 */
@Component
public class WordFilterTypeListConverter implements Converter {
    private static final Logger logger = LoggerFactory.getLogger(WordFilterTypeListConverter.class);
    public static final String DELIMETER = ",";
    public static final String START = "[";
    public static final String END = "]";

    @Resource
    private WordFilterTypeService service;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return fromString(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return toString((List<WordFilterType>) value);
    }

    public List<WordFilterType> fromString(String value) {
        try {
            logger.trace("Converting from: {}", value);
            String string = StringUtils.substringBetween(value, START, END);
            String[] filterStrings = StringUtils.split(string, DELIMETER);
            List<WordFilterType> filters = new ArrayList<WordFilterType>(filterStrings.length);
            for (String filter : filterStrings) {
                filters.add(service.get(Integer.valueOf(filter)));
            }

            logger.trace("Converted to: {}", filters);
            return filters;
        } catch (Exception e) {
            return new ArrayList<WordFilterType>(0);
        }
    }

    public String toString(List<WordFilterType> filters) {
        logger.trace("Converting from: {}", filters);
        if (filters == null) {
            return StringUtils.EMPTY;
        }

        StringBuilder sb = new StringBuilder(START);
        for (WordFilterType filter : filters) {
            sb.append(filter.getValue()).append(DELIMETER);
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append(END);

        String result = sb.toString();
        logger.trace("Converted to: {}", result);
        return result;
    }
}
