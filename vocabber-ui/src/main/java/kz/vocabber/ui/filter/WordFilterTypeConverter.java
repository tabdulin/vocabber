package kz.vocabber.ui.filter;

import kz.vocabber.api.word.WordFilterType;
import kz.vocabber.api.word.WordFilterTypeService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 * @author tabdulin
 */
@Component
public class WordFilterTypeConverter implements Converter {
    @Resource
    private WordFilterTypeService service;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return service.get(Integer.valueOf(value));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return Integer.toString(((WordFilterType) value).getValue());
    }
}
