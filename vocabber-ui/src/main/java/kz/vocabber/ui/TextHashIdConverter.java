package kz.vocabber.ui;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 * @author Talgat_Abdulin
 */
@Component
public class TextHashIdConverter implements Converter {
    private static final Logger logger = LoggerFactory.getLogger(TextHashIdConverter.class);
    
    private final String DELIMITER = "_";

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        return fromString(value);
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return toString((TextHashId) value);
    }
    
    public TextHashId fromString(String value) {
        logger.trace("Converting from: {}", value);
        TextHashId hashId = new TextHashId();
        try {
            String[] parts = StringUtils.split(value, DELIMITER);
            if (value.startsWith(DELIMITER)) {
                hashId.setId(Long.valueOf(parts[0]));
            } else if (value.endsWith(DELIMITER)) {
                hashId.setHash(parts[0]);
            } else {
                hashId.setId(Long.valueOf(parts[1]));
                hashId.setHash(parts[0]);
            }
        } catch (Exception e) {
            logger.error("Not converted");
        }
        
        logger.trace("Converted to: {}", ToStringBuilder.reflectionToString(hashId));
        return hashId;
    }
    
    public String toString(TextHashId hashId) {
        logger.trace("Converting from: {}", ToStringBuilder.reflectionToString(hashId));
        String result = "";
        try {
            result = toString(hashId.getId(), hashId.getHash());
            logger.trace("Converted to: {}", result);
        } catch (Exception e) {
            logger.error("Not converted.", e);
        }

        return result;
    }

    public String toString(Long id, String hash) {
        StringBuilder sb = new StringBuilder(StringUtils.EMPTY);
        if (hash != null) {
            sb.append(hash);
        }

        if (hash != null || id != null) {
            sb.append(DELIMITER);
        }

        if (id != null) {
            sb.append(id);
        }

        return sb.toString();
    }
}
