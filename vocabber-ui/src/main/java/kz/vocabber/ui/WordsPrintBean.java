package kz.vocabber.ui;

import kz.vocabber.api.VocabberService;
import kz.vocabber.api.comparator.WordComparator;
import kz.vocabber.api.dto.TextDTO;
import kz.vocabber.api.dto.WordDTO;
import kz.vocabber.api.dto.WordTranslationDTO;
import kz.vocabber.api.exception.VocabberException;
import kz.vocabber.api.word.WordFilterType;
import kz.vocabber.ui.service.SessionMapService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;

/**
 * @author Talgat_Abdulin
 */
@Component
@Scope("view")
public class WordsPrintBean {
    @Resource
    private VocabberService vocabberService;
    @Resource
    private SessionMapService sessionMapService;
    
    private TextHashId textId;
    private List<WordFilterType> filters;

    private TextDTO text;
    private List<WordTranslationDTO> words;

    private void init() {
        try {
            text = vocabberService.getText(textId.getId(), textId.getHash());
            if (filters != null) {
                List<WordDTO> filteredWords = vocabberService.filter(text.getWords(), filters);
                words = vocabberService.translate(filteredWords);
            } else {
                words = vocabberService.translate(text.getWords());
            }

            Collections.sort(words, new WordComparator<WordTranslationDTO>());
        } catch (VocabberException e) {
            // TODO: process
            throw new RuntimeException(e);
        }
    }

    public TextHashId getTextId() {
        return textId;
    }

    public void setTextId(TextHashId textId) {
        this.textId = textId;
    }

    public List<WordFilterType> getFilters() {
        return filters;
    }

    public void setFilters(List<WordFilterType> filters) {
        this.filters = filters;
    }

    public List<WordTranslationDTO> getWords() {
        if (words == null) {
            init();
        }

        return words;
    }
}
