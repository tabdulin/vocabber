package kz.vocabber.ui;

import kz.vocabber.api.UserService;
import kz.vocabber.api.VocabberService;
import kz.vocabber.api.dto.TextDTO;
import kz.vocabber.api.dto.WordDTO;
import kz.vocabber.api.word.WordFilterType;
import kz.vocabber.impl.word.WordFilter;
import kz.vocabber.impl.word.WordFilterFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * @author tabdulin
 */
@Component
@Scope("view")
public class FilterBean {
    @Resource
    private WordFilterFactory wordFilterFactory;
    @Resource
    private VocabberService vocabberService;
    @Resource
    private UserService userService;
    @Resource
    private MarkerBean markerBean;
    
    private WordFilterType type;
    private WordsDataTableModel words;

    public void init() {
        List<WordFilter> wordFilters = wordFilterFactory.get(Arrays.asList(type));
        Set<String> excludedWords = wordFilters.get(0).getExcludedWords();
        List<WordDTO> wordDTOs = new ArrayList<WordDTO>(excludedWords.size());
        for (String word : excludedWords) {
            WordDTO dto = new WordDTO();
            dto.setWord(word);
            wordDTOs.add(dto);
        }
        
        TextDTO text = new TextDTO();
        text.setWords(wordDTOs);
        words = new WordsDataTableModel(vocabberService, markerBean.canMark(), text, wordDTOs, true);
    }


    public WordsDataTableModel getWords() {
        if (words == null) {
            init();
        }

        return words;
    }

    public WordFilterType getType() {
        return type;
    }

    public void setType(WordFilterType type) {
        this.type = type;
    }
}
