package kz.vocabber.ui;

import kz.vocabber.api.VocabberService;
import kz.vocabber.api.comparator.FrequencyComparator;
import kz.vocabber.api.comparator.WordComparator;
import kz.vocabber.api.dto.TextDTO;
import kz.vocabber.api.dto.WordDTO;
import kz.vocabber.api.dto.WordTranslationDTO;
import kz.vocabber.api.exception.VocabberException;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import java.util.*;

/**
 * @author tabdulin
 */
public class WordsDataTableModel extends LazyDataModel<WordTranslationDTO> {
    public static final String COLUMN_WORD = "word";
    public static final String COLUMN_FREQUENCY = "frequency";

    private boolean caching = false;
    private List<WordTranslationDTO> page;
    private boolean alphabetMode = false;
    private Character letter;
    private List<Character> letters;

    private VocabberService vocabberService;
    private boolean markable;
    private List<WordDTO> words;
    private TextDTO text;
    private Map<Character, List<WordDTO>> alphabet;

    public WordsDataTableModel(VocabberService vocabberService, boolean markable,
                               TextDTO text, List<WordDTO> filteredWords,
                               boolean alphabetMode) {
        this.vocabberService = vocabberService;
        this.markable = markable;
        this.text = text;
        setWords(filteredWords);
        this.alphabetMode = alphabetMode;
        if (alphabetMode) {
            initAlphabet();
        }
    }
    
    private void initAlphabet() {
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        letters = new ArrayList<Character>(chars.length);
        for (Character letter : chars) {
            letters.add(letter);
        }

        alphabet = new HashMap<Character, List<WordDTO>>();
        for (Character letter : letters) {
            alphabet.put(letter, new LinkedList<WordDTO>());
        }

        for (WordDTO word : words) {
            char letter = word.getWord().charAt(0);
            alphabet.get(letter).add(word);
        }
        
        for (Character letter : letters) {
            if (hasLetter(letter)) {
                changeLetter(letter);
                break;
            }
        }
    }
    
    public List<Character> getLetters() {
        return letters;
    }

    public Character getLetter() {
        return letter;
    }

    public boolean renderAlphabet() {
        return alphabetMode;
    }

    public boolean hasLetter(Character letter) {
        return alphabetMode && !alphabet.get(letter).isEmpty();
    }

    public void changeLetter(Character letter) {
        this.letter = letter;
        setRowCount(alphabet.get(letter).size());
    }

    public boolean renderStatistics() {
        return !alphabetMode;
    }

    public int getWordsFound() {
        return text.getWords().size();
    }

    public int getWordsProcessed() {
        return words.size();
    }

    public List<WordDTO> getWords() {
        return words;
    }

    public void setWords(List<WordDTO> words) {
        this.words = words;
        setRowCount(words.size());
    }

    public TextDTO getText() {
        return text;
    }

    public boolean isCaching() {
        return caching;
    }

    public void setCaching(boolean caching) {
        this.caching = caching;
    }

    public List<WordTranslationDTO> getPage() {
        return page;
    }

    @Override
    public List<WordTranslationDTO> load(int first, int pageSize, String sortField, final SortOrder sortOrder, Map<String, String> filters) {
        if (caching) {
            caching = false;
            return page;
        }

        List<WordDTO> list = alphabetMode ? alphabet.get(letter) : words;
        
        if (StringUtils.equals(COLUMN_WORD, sortField) && sortOrder != SortOrder.UNSORTED) {
            Collections.sort(list, new WordComparator<WordDTO>(sortOrder == SortOrder.ASCENDING));
        } else if (StringUtils.equals(COLUMN_FREQUENCY, sortField) && sortOrder != SortOrder.UNSORTED) {
            Collections.sort(list, new FrequencyComparator<WordDTO>(sortOrder == SortOrder.ASCENDING));
        }

        List<WordDTO> wordsToTranslate = list.subList(first, first  + pageSize < getRowCount() ? first + pageSize : getRowCount());

        List<WordTranslationDTO> translatedWords = null;
        try {
            translatedWords = vocabberService.translate(wordsToTranslate);
            if (markable) {
                vocabberService.mark(translatedWords);
            }
        } catch (VocabberException e) {
            throw new RuntimeException(e);
        }

        if (StringUtils.equals(COLUMN_WORD, sortField) && sortOrder != SortOrder.UNSORTED) {
            Collections.sort(translatedWords, new WordComparator<WordTranslationDTO>(sortOrder == SortOrder.ASCENDING));
        } else if (StringUtils.equals(COLUMN_FREQUENCY, sortField) && sortOrder != SortOrder.UNSORTED) {
            Collections.sort(translatedWords, new FrequencyComparator<WordTranslationDTO>(sortOrder == SortOrder.ASCENDING));
        }

        page = translatedWords;
        return translatedWords;
    }
}
