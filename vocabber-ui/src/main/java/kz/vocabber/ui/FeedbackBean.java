package kz.vocabber.ui;

import kz.vocabber.api.UserService;
import kz.vocabber.api.VocabberService;
import kz.vocabber.api.dto.FeedbackDTO;
import kz.vocabber.api.exception.VocabberException;
import kz.vocabber.ui.service.LocalizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author Talgat_Abdulin
 */
@Component
@Lazy
@Scope("request")
public class FeedbackBean {
    private static final Logger logger = LoggerFactory.getLogger(FeedbackBean.class);

    @Resource
    private VocabberService vocabberService;
    @Resource
    private LocalizationService localizationService;
    @Resource
    private UserService userService;

    private FeedbackDTO feedback = new FeedbackDTO();

    public FeedbackDTO getFeedback() {
        return feedback;
    }

    public boolean isEmailRendered() {
        return  userService.getUserEmail() == null;
    }

    public void send() {
        try {
            logger.debug("Sending feedback ({}): \n{}", feedback.getEmail(), feedback.getContent());
            if (!isEmailRendered()) {
                feedback.setEmail(userService.getUserEmail());
            } else if (feedback.getEmail() != null) {
                // TODO: update email
            }

            vocabberService.sendFeedback(feedback);
            reset();
            localizationService.info("feedback.info.thanks");
        } catch (VocabberException e) {
            localizationService.error(e);
        }
    }

    public void cancel() {
        reset();
    }

    public void reset() {
        feedback = new FeedbackDTO();
    }
}
