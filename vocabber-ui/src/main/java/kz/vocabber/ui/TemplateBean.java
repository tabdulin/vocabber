package kz.vocabber.ui;

import kz.vocabber.api.UserService;
import kz.vocabber.api.dto.UserDTO;
import kz.vocabber.impl.users.UserRole;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author Talgat_Abdulin
 */
@Component
public class TemplateBean {
    @Resource
    private UserService userService;

    public boolean isLoginRendered() {
        return userService.getCurrentUser().getRole() == UserRole.ROLE_ANONYMOUS;
    }
    
    public UserDTO getUser() {
        return userService.getCurrentUser();
    }
}
