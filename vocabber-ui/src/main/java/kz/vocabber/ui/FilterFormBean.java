package kz.vocabber.ui;

import kz.vocabber.api.UserService;
import kz.vocabber.api.word.WordFilterType;
import kz.vocabber.api.word.WordFilterTypeService;
import kz.vocabber.ui.filter.WordFilterTypeItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author tabdulin
 */
@Component
@Scope("view")
public class FilterFormBean {
    private static final Logger logger = LoggerFactory.getLogger(FilterFormBean.class);

    @Resource
    private UserService userService;
    @Resource
    private WordFilterTypeService wordFilterTypeService;

    private List<WordFilterTypeItem> filters;

    @PostConstruct
    public void init() {
        logger.debug("Initializing filterFormBean");
        filters = new ArrayList<WordFilterTypeItem>();
        for (WordFilterType filter : wordFilterTypeService.get()) {
            filters.add(new WordFilterTypeItem(filter, false));
        }
        logger.debug("filterFormBean were initialized");
    }

    public List<WordFilterTypeItem> getFilters() {
        return filters;
    }

    public List<WordFilterType> getSelectedFilters() {
        List<WordFilterType> selected = new ArrayList<WordFilterType>(filters.size());
        for (WordFilterTypeItem filterItem : filters) {
            if (filterItem.isSelected()) {
                selected.add(filterItem.getFilter());
            }
        }
        return selected;
    }

    public void setSelectedFilters(List<WordFilterType> selectedFilters) {
        for (WordFilterTypeItem filterItem : filters) {
            filterItem.setSelected(false);
            if (selectedFilters != null && selectedFilters.contains(filterItem.getFilter())) {
                filterItem.setSelected(true);
            }
        }
    }

    public void reset() {
        setSelectedFilters(null);
    }
}
