package kz.vocabber.ui.exception;

import kz.vocabber.ui.service.UrlService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.FacesException;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.event.ExceptionQueuedEvent;
import java.util.Iterator;

/**
 * @author tabdulin
 */
public class ExceptionHandler extends ExceptionHandlerWrapper {
    private static final Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);

    private javax.faces.context.ExceptionHandler wrapped;
    private UrlService urlService;

    public ExceptionHandler(javax.faces.context.ExceptionHandler wrapped) {
        urlService = new UrlService();
        this.wrapped = wrapped;
    }

    @Override
    public javax.faces.context.ExceptionHandler getWrapped() {
        return wrapped;
    }

    @Override
    public void handle() throws FacesException {
        Iterator<ExceptionQueuedEvent> iterator = getUnhandledExceptionQueuedEvents().iterator();
        while (iterator.hasNext()) {
            try {
                ExceptionQueuedEvent event = iterator.next();
                logger.error("Processing error", event.getContext().getException());
                if (event.getContext().getException() instanceof ViewExpiredException) {
                    urlService.reload();
                }
            } finally {
                iterator.remove();;
            }
        };

        super.handle();
    }
}
