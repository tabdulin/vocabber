package kz.vocabber.ui;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Talgat_Abdulin
 */
public class TextHashIdConverterTest {
    TextHashIdConverter converter = new TextHashIdConverter();
    
    @Test
    public void testFromString() throws Exception {
        String id1 = "99a8f2a6e58174142f129d091e2f8512_95";
        TextHashId hashId1 = converter.fromString(id1);
        Assert.assertEquals((long) hashId1.getId(), 95l);
        Assert.assertEquals(hashId1.getHash(), "99a8f2a6e58174142f129d091e2f8512");

        String id2 = null;
        TextHashId hashId2 = converter.fromString(id2);
        Assert.assertNull(hashId2.getId());
        Assert.assertEquals(hashId2.getHash(), null);

        String id3 = "hash_";
        TextHashId hashId3 = converter.fromString(id3);
        Assert.assertNull(hashId3.getId());
        Assert.assertEquals(hashId3.getHash(), "hash");

        String id4 = "_9";
        TextHashId hashId4 = converter.fromString(id4);
        Assert.assertEquals(9l, (long) hashId4.getId());
        Assert.assertNull(hashId4.getHash());
    }

    @Test
    public void testToString() throws Exception {
        Assert.assertEquals("", converter.toString(null, null));
        Assert.assertEquals("_9", converter.toString(9l, null));
        Assert.assertEquals("hash_", converter.toString(null, "hash"));
        Assert.assertEquals("hash_9", converter.toString(9l, "hash"));
    }
}
