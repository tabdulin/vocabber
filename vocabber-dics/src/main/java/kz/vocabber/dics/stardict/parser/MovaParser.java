package kz.vocabber.dics.stardict.parser;

import kz.vocabber.db.model.word.Translation;
import kz.vocabber.db.model.word.Word;
import kz.vocabber.dics.DictionaryParser;
import kz.vocabber.dics.DictionaryType;
import kz.vocabber.dics.stardict.StardictDictionaryType;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author tabdulin
 */
@Service
public class MovaParser implements DictionaryParser {
    private static final Logger logger = LoggerFactory.getLogger(MovaParser.class);

    @Override
    public DictionaryType getDictionaryType() {
        return StardictDictionaryType.MOVA_EN_RU;
    }

    @Override
    public Word parse(String word, String translation) {
        logger.trace("Word '{}' translation: {}", word, translation);
        String[] lines = StringUtils.split(translation, "\n");
        if (lines == null) {
            return null;
        }


        Set<Translation> translations = new LinkedHashSet<Translation>();
        Word w = new Word();
        w.setWord(word);

        for (String line : lines) {
            if (StringUtils.contains(line, word) || StringUtils.isBlank(line)) {
                continue;
            }

            Translation tran = new Translation();
            tran.setWord(w);
            tran.setDictionaryType(getDictionaryType().getValue());
            tran.setTranslation(StringUtils.trim(line));
            translations.add(tran);
        }

        w.setTranslations(translations);
        return w;
    }
}
