package kz.vocabber.dics.stardict;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author tabdulin
 */
@Service
public class DictionaryFactory {
    private String stardictHome = //"D:\\vocabber\\home\\dics\\stardict-Mueller7accentGPL-2.4.2\\";
                "/home/tabdulin/projects/wordext/home/stardict/";

    private Map<StardictDictionaryType,Dictionary> dictionaries =
            new HashMap<StardictDictionaryType, Dictionary>(StardictDictionaryType.values().length);

    public Dictionary getDictionary(StardictDictionaryType dictionaryType) {
        if (!dictionaries.containsKey(dictionaryType)) {
            dictionaries.put(dictionaryType, new Dictionary(stardictHome + dictionaryType.getFile()));
        }

        return dictionaries.get(dictionaryType);
    }
}
