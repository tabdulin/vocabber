package kz.vocabber.dics.stardict.loader;

import kz.vocabber.dics.DictionaryType;
import kz.vocabber.dics.stardict.StardictDictionaryType;
import org.springframework.stereotype.Service;

/**
 * @author tabdulin
 */
@Service
public class MovaLoader extends StardictDictionaryLoader {
    @Override
    public DictionaryType getDictionaryType() {
        return StardictDictionaryType.MOVA_EN_RU;
    }
}
