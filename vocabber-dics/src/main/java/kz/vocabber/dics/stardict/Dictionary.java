package kz.vocabber.dics.stardict;

/**
 * @author tabdulin
 */
public class Dictionary {
    private DictionaryInfo info;
    private DictionaryIndex index;
    private DictionaryData data;

    Dictionary(String file) {
        info = new DictionaryInfo(file);
        index = new DictionaryIndex(info);
        data = new DictionaryData(info);
    }


    public String translate(String word) {
        WordLocation wordLocation = index.search(word);
        return data.getTranslation(wordLocation);
    }

    public DictionaryInfo getInfo() {
        return info;
    }

    public DictionaryIndex getIndex() {
        return index;
    }

    public DictionaryData getData() {
        return data;
    }
}
