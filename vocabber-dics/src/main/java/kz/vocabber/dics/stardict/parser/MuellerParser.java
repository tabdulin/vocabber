package kz.vocabber.dics.stardict.parser;

import kz.vocabber.dics.DictionaryParser;
import kz.vocabber.dics.DictionaryType;
import kz.vocabber.dics.stardict.StardictDictionaryType;
import kz.vocabber.db.model.word.Transcription;
import kz.vocabber.db.model.word.Translation;
import kz.vocabber.db.model.word.Word;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author tabdulin
 */
@Service
public class MuellerParser extends XdxfDictionaryParser implements DictionaryParser {
    private static final Logger logger = LoggerFactory.getLogger(MuellerParser.class);

    private Map<String, Translation.Type> types;

    public MuellerParser() {
        types = new HashMap<String, Translation.Type>(Translation.Type.values().length);
        types.put("<abr>_n.</abr>", Translation.Type.NOUN);
        types.put("<abr>_v.</abr>", Translation.Type.VERB);
        types.put("<abr>_a.</abr>", Translation.Type.ADJECTIVE);
        types.put("<abr>_adv.</abr>", Translation.Type.ADVERB);
    }

    @Override
    public DictionaryType getDictionaryType() {
        return StardictDictionaryType.MUELLER;
    }

    @Override
    public Word parse(String word, String translation) {
        String[] lines = StringUtils.split(translation, "\n");
        if (lines == null) {
            return null;
        }

        Set<Transcription> transcriptions = new LinkedHashSet<Transcription>();
        Set<Translation> translations = new LinkedHashSet<Translation>();
        Word w = new Word();
        w.setWord(StringUtils.lowerCase(word));

        Translation.Type currentType = Translation.Type.UNKNOWN;
        for (String line : lines) {
            if (StringUtils.isBlank(line)) {
                continue;
            }
            
            line = line.trim().toLowerCase();

            // skip article
            if (line.startsWith("<k>")) {
                continue;
            }

            // process transcription
            if (StringUtils.contains(line, TRANSCRIPTION_TAG_START)) {
                String value = StringUtils.substringBetween(line, TRANSCRIPTION_TAG_START, TRANSCRIPTION_TAG_END);
                if (StringUtils.isBlank(value)) {
                    continue;
                }

                Transcription transcription = new Transcription();
                transcription.setWord(w);
                transcription.setTranscription(StringUtils.trim(value));
                transcription.setDictionaryType(getDictionaryType().getValue());
                transcriptions.add(transcription);
                continue;
            }

            if (lines.length > 3) { // multiple translations
                // process word type (noun)
                currentType = processType(currentType, line);

                // skip non translations
                if (!line.startsWith("<b>")) {
                    continue;
                } else {
                    String point = StringUtils.substringBetween(line, "<b>", "&gt;</b>");
                    if (!StringUtils.isNumeric(point)) {
                        continue;
                    }
                }

                line = removeB(line);
                line = removeEX(line);
                line = removeABR(line);
            } else { // single translation
                currentType = processType(currentType, line);
                line = removeABR(line);
            }

            line = processDelimeters(line);

            if (StringUtils.isBlank(line)) {
                continue;
            }

            Translation tran = new Translation();
            tran.setWord(w);
            tran.setType(currentType);
            tran.setDictionaryType(getDictionaryType().getValue());
            tran.setTranslation(StringUtils.trim(line));
            translations.add(tran);
        }

        w.setTranscriptions(transcriptions);
        w.setTranslations(translations);
        return w;
    }

    private Translation.Type processType(Translation.Type currentType, String line) {
        Translation.Type type = getType(line);
        if (type != Translation.Type.UNKNOWN) {
            currentType = type;
        }

        return currentType;
    }

    private String processDelimeters(String line) {
        line = StringUtils.removeEnd(line, ";");
        line = StringUtils.replace(line, ";", ",");
        return line;
    }

    private String removeB(String line) {
        if (StringUtils.contains(line, B_TAG_END)) {
            return StringUtils.substringAfter(line, B_TAG_END);
        }
        return line;
    }

    private String removeEX(String line) {
        if (StringUtils.contains(line, EX_TAG_START)) {
            line = StringUtils.substringBefore(line, EX_TAG_START);
        }
        return line;
    }

    private String removeABR(String line) {
        if (StringUtils.contains(line, ABR_TAG_START)
                || StringUtils.contains(line, ABR_TAG_END)) {
            int matches = Math.max(StringUtils.countMatches(line, ABR_TAG_START),
                    StringUtils.countMatches(line, ABR_TAG_END));
            for (int i = 0; i < matches; i++) {
                String abr = StringUtils.substringBetween(line, ABR_TAG_START, ABR_TAG_END);
                if (StringUtils.startsWith(abr, "_")) {
                    line = StringUtils.remove(line, ABR_TAG_START + abr + ABR_TAG_END);
                } else {
                    line = StringUtils.replaceOnce(line, ABR_TAG_START, StringUtils.EMPTY);
                    line = StringUtils.replaceOnce(line, ABR_TAG_END, StringUtils.EMPTY);
                }
            }
        }
        return line;
    }

    private Translation.Type getType(String line) {
        for (String key : types.keySet()) {
            if (line.contains(key)) {
                return types.get(key);
            }
        }

        return Translation.Type.UNKNOWN;
    }
}
