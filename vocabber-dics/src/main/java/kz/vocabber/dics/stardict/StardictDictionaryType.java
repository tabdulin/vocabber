package kz.vocabber.dics.stardict;

import kz.vocabber.dics.DictionaryType;

/**
 * @author tabdulin
 */
public enum StardictDictionaryType implements DictionaryType {
    LINGVO_UNIVERSAL_EN_RU("dictionary.kz.vocabber.dics.stardict.lingvo", "LingvoUniversalEnRu"),
    LONGMAN("dictionary.kz.vocabber.dics.stardict.longman", "longman"),
    MUELLER("dictionary.kz.vocabber.dics.stardict.mueller", "Mueller7accentGPL"),
    TRANSCRIPTION("dictionary.kz.vocabber.dics.stardict.transcription", "cmupd"),
    FREEDICT_DE_EN_RU("dictionary.kz.vocabber.dics.kz.vocabber.dics.stardict.freedict", "dictd_www.freedict.de_eng-rus"),
    MOVA_EN_RU("dictionary.kz.vocabber.dics.stardict.mova", "dictd_www.mova.org_slovnyk_en-ru"),
    SLOVNYK_EN_RU("dictionary.kz.vocabber.dics.kz.vocabber.dics.stardict.slovnyk", "slovnyk_en-ru");

    private String file;
    private String code;

    private StardictDictionaryType(String code, String file) {
        this.file = file;
        this.code = code;
    }

    public String getFile() {
        return file;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public int getValue() {
        return 100 + ordinal();
    }


}
