package kz.vocabber.dics.stardict.loader;

import kz.vocabber.dics.DictionaryType;
import kz.vocabber.dics.stardict.StardictDictionaryType;
import org.springframework.stereotype.Service;

/**
 * @author tabdulin
 */
@Service
public class MuellerLoader extends StardictDictionaryLoader {
    @Override
    public DictionaryType getDictionaryType() {
        return StardictDictionaryType.MUELLER;
    }
}
