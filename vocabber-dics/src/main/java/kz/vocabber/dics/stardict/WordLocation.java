package kz.vocabber.dics.stardict;

/**
* @author tabdulin
*/
public class WordLocation {
    private String word;
    private int offset;
    private int size;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
