package kz.vocabber.dics.stardict.loader;

import kz.vocabber.dics.DictionaryType;
import kz.vocabber.dics.stardict.StardictDictionaryType;
import org.springframework.stereotype.Service;

/**
 * @author tabdulin
 */
@Service
public class LingvoLoader extends StardictDictionaryLoader {

    @Override
    public DictionaryType getDictionaryType() {
        return StardictDictionaryType.LINGVO_UNIVERSAL_EN_RU;
    }
}
