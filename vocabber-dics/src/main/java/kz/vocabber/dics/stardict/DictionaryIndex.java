package kz.vocabber.dics.stardict;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;

/**
 * Wrapper for *.idx file
 *
 * @author tabdulin
 */
public class DictionaryIndex {
    private static final Logger logger = LoggerFactory.getLogger(DictionaryIndex.class);

    private RandomAccessFile file;
    private DictionaryInfo info;

    public DictionaryIndex(DictionaryInfo info) {
        try {
            this.info = info;
            file = new RandomAccessFile(info.getFileName() + ".idx", "r");
        } catch (FileNotFoundException e) {
            throw new RuntimeException("File " + info.getFileName() + " was not found");
        }
    }

    public WordLocation search(String word) {
        word = StringUtils.lowerCase(word);

        int left = 0;
        int middle = 0;
        int right = info.getIdxFileSize();
        WordLocation location = null;
        while (left <= right) {
            middle = (left + right) / 2;
            location = getNextWord(middle);

            if (location == null) {
                return null;
            }

            if (StringUtils.isBlank(location.getWord())) {
                left += 2;
                continue;
            }

            if (location.getWord().compareTo(word) > 0) {
                right = middle - 1;
            } else if (location.getWord().compareTo(word) < 0) {
                left = middle + 1;
            } else {
                break;
            }
        }

        return StringUtils.equals(word, location.getWord()) ? location : null;
    }

    /**
     * http://code.google.com/p/babiloo/wiki/StarDict_format#The_".idx"_file's_format
     *
     * @param position
     * @return
     */
    public WordLocation getNextWord(int position) {
        try {
            file.seek(position);

            byte[] buffer = new byte[(256 + 8) * 2]; // 256  byte max word length + 32 bit number + 32 bit number
            file.read(buffer);
            for (int i = 0; i < buffer.length; i++) {
                if (CharUtils.isAsciiAlpha((char) buffer[i])
                        && i + 10 < buffer.length
                        && buffer[i + 1] == '\0'
                        && CharUtils.isAsciiAlpha((char) buffer[i + 10])) {

                    int WORD_POSITION_START = i + 10;
                    for (int j = WORD_POSITION_START; j < buffer.length; j++) {
                        if (buffer[j] == '\0') {
                            int WORD_POSITION_END = j;
                            WordLocation wordLocation = new WordLocation();
                            wordLocation.setWord(new String(
                                    Arrays.copyOfRange(buffer, WORD_POSITION_START, WORD_POSITION_END))
                                    .toLowerCase());


                            int offset = 0;
                            int OFFSET_POSITION_START = WORD_POSITION_END + 1;
                            int OFFSET_POSITION_END = OFFSET_POSITION_START + 4;
                            for (int k = OFFSET_POSITION_START; k < OFFSET_POSITION_END; k++) {
                                offset <<= 8;
                                offset |= buffer[k] & 0xff;
                            }
                            wordLocation.setOffset(offset);

                            int size = 0;
                            int SIZE_POSITION_START = OFFSET_POSITION_END;
                            int SIZE_POSITION_END = SIZE_POSITION_START + 4;
                            for (int k = SIZE_POSITION_START; k < SIZE_POSITION_END; k++) {
                                size <<= 8;
                                size |= buffer[k] & 0xff;
                            }
                            wordLocation.setSize(size);

                            return wordLocation;
                        }
                    }
                }
            }
        } catch (IOException e) {
            logger.error("Cannot access file", e);
        }

        return null;
    }


    public Iterator getIterator() {
        return new Iterator();
    }

    @Override
    protected void finalize() throws Throwable {
        IOUtils.closeQuietly(file);
    }

    /**
         * @author tabdulin
         */
    public class Iterator implements java.util.Iterator<WordLocation> {
        private int wordCounter = 0;
        private int position = 0;

        @Override
        public boolean hasNext() {
            return wordCounter < info.getWordCount();
        }

        @Override
        public WordLocation next() {
            try {
                file.seek(position);

                byte[] buffer = new byte[256 + 8]; // 256  byte max word length + 32 bit number + 32 bit number
                file.read(buffer);
                int WORD_POSITION_START = 0;
                for (int j = WORD_POSITION_START; j < buffer.length; j++) {
                    if (buffer[j] == '\0') {
                        int WORD_POSITION_END = j;
                        WordLocation wordLocation = new WordLocation();
                        wordLocation.setWord(new String(
                                Arrays.copyOfRange(buffer, WORD_POSITION_START, WORD_POSITION_END)));


                        int offset = 0;
                        int OFFSET_POSITION_START = WORD_POSITION_END + 1;
                        int OFFSET_POSITION_END = OFFSET_POSITION_START + 4;
                        for (int k = OFFSET_POSITION_START; k < OFFSET_POSITION_END; k++) {
                            offset <<= 8;
                            offset |= buffer[k] & 0xff;
                        }
                        wordLocation.setOffset(offset);

                        int size = 0;
                        int SIZE_POSITION_START = OFFSET_POSITION_END;
                        int SIZE_POSITION_END = SIZE_POSITION_START + 4;
                        for (int k = SIZE_POSITION_START; k < SIZE_POSITION_END; k++) {
                            size <<= 8;
                            size |= buffer[k] & 0xff;
                        }
                        wordLocation.setSize(size);

                        wordCounter++;
                        position += SIZE_POSITION_END;
                        return wordLocation;
                    }
                }
            } catch (IOException e) {
                logger.error("Cannot access file", e);
            }

            return null;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
