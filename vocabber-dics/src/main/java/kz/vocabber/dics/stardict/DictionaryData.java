package kz.vocabber.dics.stardict;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Wrapper for *.dict.dz file
 *
 * @author tabdulin
 */
public class DictionaryData {
    private static final Logger logger = LoggerFactory.getLogger(DictionaryData.class);

    private DictZipFile file;

    public DictionaryData(DictionaryInfo info) {
        file = new DictZipFile(info.getFileName() + ".dict.dz");
    }

    public String getTranslation(WordLocation wordLocation) {
        if (wordLocation == null) {
            return null;
        }

        byte[] buffer = new byte[wordLocation.getSize()];
        file.seek(wordLocation.getOffset());
        try {
            file.read(buffer, wordLocation.getSize());
            return new String(buffer);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return null;
    }

    @Override
    protected void finalize() throws Throwable {
        IOUtils.closeQuietly(file);
    }
}
