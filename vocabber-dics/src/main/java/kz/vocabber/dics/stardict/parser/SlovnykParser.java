package kz.vocabber.dics.stardict.parser;

import kz.vocabber.dics.DictionaryParser;
import kz.vocabber.dics.DictionaryType;
import kz.vocabber.dics.stardict.StardictDictionaryType;
import kz.vocabber.db.model.word.Transcription;
import kz.vocabber.db.model.word.Translation;
import kz.vocabber.db.model.word.Word;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author tabdulin
 */
@Service
public class SlovnykParser extends XdxfDictionaryParser implements DictionaryParser {

    protected static final String QUOT = "&quot;";
    protected static final String K_TAG_START = "<k>";

    @Override
    public DictionaryType getDictionaryType() {
        return StardictDictionaryType.SLOVNYK_EN_RU;
    }

    @Override
    public Word parse(String word, String translation) {
        if (translation == null) {
            return null;
        }

        Set<Transcription> transcriptions = new LinkedHashSet<Transcription>();
        Set<Translation> translations = new LinkedHashSet<Translation>();
        Word w = new Word();
        w.setWord(StringUtils.lowerCase(word));

        int matches = StringUtils.countMatches(translation, K_TAG_START);
        for (int i = 0; i < matches; i++) {
            if (i > 0) {
                int nextK = StringUtils.indexOf(translation, K_TAG_START, K_TAG_START.length());
                translation = StringUtils.substring(translation, nextK);
            }

            String value = StringUtils.substringBetween(translation, QUOT);
            if (StringUtils.isBlank(value)) {
                return null;
            }

            Translation tran = new Translation();
            tran.setWord(w);
            tran.setDictionaryType(getDictionaryType().getValue());
            tran.setTranslation(value.trim().toLowerCase());
            translations.add(tran);
        }



        w.setTranscriptions(transcriptions);
        w.setTranslations(translations);
        return w;
    }
}
