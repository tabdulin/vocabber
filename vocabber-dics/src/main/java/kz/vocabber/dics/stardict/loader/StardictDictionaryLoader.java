package kz.vocabber.dics.stardict.loader;

import kz.vocabber.db.model.word.Transcription;
import kz.vocabber.db.model.word.Word;
import kz.vocabber.db.model.word.Translation;
import kz.vocabber.dics.DictionaryLoader;
import kz.vocabber.dics.DictionaryParserFactory;
import kz.vocabber.dics.DictionaryType;
import kz.vocabber.dics.stardict.*;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author tabdulin
 */
public abstract class StardictDictionaryLoader implements DictionaryLoader {
    private static final Logger logger = LoggerFactory.getLogger(StardictDictionaryLoader.class);

    @Resource
    private DictionaryParserFactory dictionaryParserFactory;
    @Resource
    private DictionaryFactory dictionaryFactory;
    @Resource
    private SessionFactory sessionFactory;

    public abstract DictionaryType getDictionaryType();

    @Override
    @Transactional
    public void load() {
        Dictionary dictionary = dictionaryFactory.getDictionary((StardictDictionaryType) getDictionaryType());
        DictionaryIndex.Iterator iterator = dictionary.getIndex().getIterator();
        int i = 0;
        Session session = sessionFactory.getCurrentSession();
        session.setFlushMode(FlushMode.MANUAL);
        while (iterator.hasNext()) {
            WordLocation location = iterator.next();
            logger.info("Word: {}", location.getWord());
            String translation = dictionary.getData().getTranslation(location);

            try {
                Word word = dictionaryParserFactory.getParser(getDictionaryType())
                        .parse(location.getWord(), translation);

                if (word != null) {
                    @SuppressWarnings("unchecked")
                    Word existingWord  = (Word) session.createQuery(
                            "from Word word " +
                            "where word.word = :word")
                            .setParameter("word", word.getWord())
                            .uniqueResult();

                    if (existingWord == null) {
                        session.persist(word);
                        continue;
                    }

                    // TODO: remove hack
                    if (true) continue;
                    for (Transcription transcription : word.getTranscriptions()) {
                        transcription.setWord(existingWord);
                        session.persist(transcription);
                    }

                    for (Translation t : word.getTranslations()) {
                        t.setWord(existingWord);
                        session.persist(t);
                    }
                }
            } catch (ConstraintViolationException e) {
                logger.warn("Word '{}' already exists", location.getWord());
            }

            if (++i % 1000 == 0) {
                session.flush();
            }
        }

        session.flush();
    }
}
