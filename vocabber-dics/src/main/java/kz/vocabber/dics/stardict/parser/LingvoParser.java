package kz.vocabber.dics.stardict.parser;

import kz.vocabber.dics.DictionaryParser;
import kz.vocabber.dics.DictionaryType;
import kz.vocabber.dics.stardict.StardictDictionaryType;
import kz.vocabber.db.model.word.Transcription;
import kz.vocabber.db.model.word.Translation;
import kz.vocabber.db.model.word.Word;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * @author tabdulin
 */
@Service("lingvoParser")
public class LingvoParser extends XdxfDictionaryParser implements DictionaryParser {
    private static final Logger logger = LoggerFactory.getLogger(LingvoParser.class);


    private List<String> TAGS = Arrays.asList(TRANSCRIPTION_TAG_NAME, TRANSLATION_TAG_NAME);

    @Override
    public DictionaryType getDictionaryType() {
        return StardictDictionaryType.LINGVO_UNIVERSAL_EN_RU;
    }

    @Override
    public Word parse(String word, String translation) {
        try {
            Document document = DocumentHelper.parseText(StringUtils.join("<xdxf>", translation, "</xdxf>"));
            Element root = document.getRootElement();
            clean(root);

            Set<Transcription> transcriptions = new LinkedHashSet<Transcription>();
            Set<Translation> translations = new LinkedHashSet<Translation>();
            Word w = new Word();
            w.setWord(word);
            for (Element child : (List<Element>) root.elements()) {
                String value = child.getTextTrim();
                if (StringUtils.isBlank(value)) {
                    continue;
                }

                if (StringUtils.equals(TRANSCRIPTION_TAG_NAME, child.getName())) {
                    Transcription tr = new Transcription();
                    tr.setDictionaryType(getDictionaryType().getValue());
                    tr.setTranscription(value);
                    tr.setWord(w);
                    transcriptions.add(tr);
                } else if (StringUtils.equals(TRANSLATION_TAG_NAME, child.getName())) {
                    Translation dtrn = new Translation();
                    dtrn.setDictionaryType(getDictionaryType().getValue());
                    dtrn.setTranslation(value);
                    dtrn.setWord(w);
                    translations.add(dtrn);
                }
            }

            w.setTranscriptions(transcriptions);
            w.setTranslations(translations);
            return w;
        } catch (DocumentException e) {
            logger.error("Translation was not parsed", e);
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    private void clean(Element element) {
        for (Element child : (List<Element>) element.elements()) {
            if (!TAGS.contains(child.getName())) {
                element.remove(child);
            } else {
                clean(child);
            }
        }
    }
}
