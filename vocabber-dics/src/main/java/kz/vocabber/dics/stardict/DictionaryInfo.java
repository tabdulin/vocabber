package kz.vocabber.dics.stardict;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrTokenizer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;

/**
 * Wrapper for *.ifo file
 *
 * @author tabdulin
 */
public class DictionaryInfo {
    private String fileName;

    private String version;
    private String bookName;
    private Integer wordCount;
    private Integer synWordCount;
    private Integer idxFileSize;
    private IdxOffsetBits idxOffsetBits;
    private SameTypeSequence sameTypeSequence;

    private String author;
    private String email;
    private String description;
    private String date;

    public DictionaryInfo(String fileName) {
        this.fileName = fileName;
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName + ".ifo"));
            String line = br.readLine();
            while (line != null) {
                if (!line.contains("=")) {
                    line = br.readLine();
                    continue;
                }

                for (Field field : getClass().getDeclaredFields()) {
                    StrTokenizer tokenizer = new StrTokenizer(line, "=");
                    if (StringUtils.equalsIgnoreCase(field.getName(), tokenizer.getTokenList().get(0))) {
                        String value = tokenizer.getTokenList().get(1);
                        if (field.getType() == Integer.class) {
                            field.set(this, Integer.parseInt(value));
                        } else if (field.getType() == IdxOffsetBits.class) {
                            field.set(this,  IdxOffsetBits.valueOf(value));
                        } else if (field.getType() == SameTypeSequence.class) {
                            field.set(this,  SameTypeSequence.valueOf(value));
                        } else {
                            field.set(this, value);
                        }

                    }
                }

                line = br.readLine();
            }

            IOUtils.closeQuietly(br);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }

    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public Integer getWordCount() {
        return wordCount;
    }

    public void setWordCount(Integer wordCount) {
        this.wordCount = wordCount;
    }

    public Integer getSynWordCount() {
        return synWordCount;
    }

    public void setSynWordCount(Integer synWordCount) {
        this.synWordCount = synWordCount;
    }

    public Integer getIdxFileSize() {
        return idxFileSize;
    }

    public void setIdxFileSize(Integer idxFileSize) {
        this.idxFileSize = idxFileSize;
    }

    public IdxOffsetBits getIdxOffsetBits() {
        return idxOffsetBits;
    }

    public void setIdxOffsetBits(IdxOffsetBits idxOffsetBits) {
        this.idxOffsetBits = idxOffsetBits;
    }

    public SameTypeSequence getSameTypeSequence() {
        return sameTypeSequence;
    }

    public void setSameTypeSequence(SameTypeSequence sameTypeSequence) {
        this.sameTypeSequence = sameTypeSequence;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public static enum IdxOffsetBits {
        B_32,
        B_64
    }

    public static enum SameTypeSequence {
        m,
        l,
        g,
        t,
        x,
        y,
        k,
        w,
        h,
        r,
        W,
        P,
        X
    }
}
