package kz.vocabber.dics.stardict.parser;

/**
 * @author tabdulin
 */
public class XdxfDictionaryParser {
    protected static final String TRANSCRIPTION_TAG_NAME = "tr";
    protected static final String TRANSCRIPTION_TAG_START = "<tr>";
    protected static final String TRANSCRIPTION_TAG_END = "</tr>";

    protected static final String TRANSLATION_TAG_NAME = "dtrn";

    protected static final String B_TAG_END = "</b>";
    protected static final String EX_TAG_START = "<ex>";
    protected static final String ABR_TAG_START = "<abr>";
    protected static final String ABR_TAG_END = "</abr>";

}
