package kz.vocabber.dics;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author tabdulin
 */
@Service
public class DictionaryParserFactory {
    @Resource
    private ApplicationContext applicationContext;

    public DictionaryParser getParser(DictionaryType type) {
        Map<String, DictionaryParser> parsers = applicationContext.getBeansOfType(DictionaryParser.class);

        for (DictionaryParser parser : parsers.values()) {
            if (type.getValue() == parser.getDictionaryType().getValue()) {
                return parser;
            }
        }

        throw new IllegalArgumentException("Unsupported dictionary type: " + type.getCode());
    }
}
