package kz.vocabber.dics;

import kz.vocabber.db.model.word.Word;

/**
 * @author tabdulin
 */
public interface DictionaryParser {
    /**
         * @return dictionary type of the parser
         */
    DictionaryType getDictionaryType();

    /**
         * Parses translation string and builds a Word object with translations data
         *
         * @param word
         * @param translation
         * @return
         */
    Word parse(String word, String translation);
}
