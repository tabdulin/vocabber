package kz.vocabber.dics;

/**
 * @author tabdulin
 */
public interface DictionaryLoader {
    /**
         * Loads dictionary data into database
         */
    void load();
}
