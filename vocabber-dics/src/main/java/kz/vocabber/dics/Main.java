package kz.vocabber.dics;

import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;

import javax.naming.NamingException;

/**
 * @author tabdulin
 */
public class Main {
    public static void main(String... args) {
        try {
            SimpleNamingContextBuilder builder = new SimpleNamingContextBuilder();
            PGSimpleDataSource ds = new PGSimpleDataSource();
            ds.setDatabaseName("vocabberdb");
            ds.setUser("vocabber");
            ds.setPassword("vocabber");

            builder.bind("java:comp/env/jdbc/DataSource", ds);
            builder.activate();

            ApplicationContext context = new ClassPathXmlApplicationContext("/dicsApplicationContext.xml");

            DictionaryLoader loader = (DictionaryLoader) context.getBean("muellerLoader");
            loader.load();
        } catch (NamingException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
