package kz.vocabber.dics;

import kz.vocabber.dics.stardict.StardictDictionaryType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author tabdulin
 */
@Service
public class DictionaryTypeService {
    public static final DictionaryType NO_DICTIONARY = new DictionaryType() {
        @Override
        public String getCode() {
            return "dictionary.none";
        }

        @Override
        public int getValue() {
            return 0;
        }
    };

    /**
         * @return supported dictionary types
         */
    public List<DictionaryType> get() {
        ArrayList<DictionaryType> dics = new ArrayList<DictionaryType>();
        dics.add(NO_DICTIONARY);
        dics.addAll(Arrays.asList(
                StardictDictionaryType.LINGVO_UNIVERSAL_EN_RU,
                StardictDictionaryType.MUELLER,
                StardictDictionaryType.MOVA_EN_RU));
        return dics;
    }

    public DictionaryType get(String code) {
        for (DictionaryType type : get()) {
            if (StringUtils.equalsIgnoreCase(type.getCode(), code)) {
                return type;
            }
        }

        throw new IllegalArgumentException("Unsupported dictionary type code: " + code);
    }

    public DictionaryType get(int value) {
        for(DictionaryType type : get()) {
            if (type.getValue() == value) {
                return type;
            }
        }

        throw new IllegalArgumentException("Unsupported dictionary type value: " + value);
    }

    public boolean isStardict(DictionaryType dictionaryType) {
        return dictionaryType instanceof StardictDictionaryType;
    }
}
