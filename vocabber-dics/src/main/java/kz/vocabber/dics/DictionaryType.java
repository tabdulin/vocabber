package kz.vocabber.dics;

/**
 * @author tabdulin
 */
public interface DictionaryType {
    String getCode();

    int getValue();
}
