package kz.vocabber.dics.stardict;

import org.junit.Ignore;
import org.junit.Test;

/**
 * @author tabdulin
 */
// TODO: fix
@Ignore
public class DictionaryDataTest {

    @Test
    public void getTranslation() {
        DictionaryInfo info = new DictionaryInfo("src/test/resources/stardict/cmupd");
        DictionaryIndex index = new DictionaryIndex(info);
        DictionaryData data = new DictionaryData(info);

        data.getTranslation(index.getNextWord(2230));
        data.getTranslation(index.getNextWord(8000));
    }
}
