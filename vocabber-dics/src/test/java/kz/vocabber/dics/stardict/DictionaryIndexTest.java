package kz.vocabber.dics.stardict;

import junit.framework.Assert;
import org.junit.Ignore;
import org.junit.Test;

/**
 * @author tabdulin
 */
// TODO: fix
@Ignore
public class DictionaryIndexTest {
    DictionaryIndex index;

    public DictionaryIndexTest() {
        DictionaryInfo info = new DictionaryInfo("src/test/resources/stardict/cmupd");
        index = new DictionaryIndex(info);
    }

    @Test
    public void getClosestWord() throws Exception {

        WordLocation wordLocation = index.getNextWord(2230);

        Assert.assertEquals("abend", wordLocation.getWord());
        Assert.assertEquals(1405, wordLocation.getOffset());
        Assert.assertEquals(7, wordLocation.getSize());

    }

    @Test
    public void search() {
        String word = "idea";
        WordLocation wordLocation = index.search(word);
        Assert.assertEquals(word, wordLocation.getWord());

        word = "demand";
        wordLocation = index.search(word);
        Assert.assertEquals(word, wordLocation.getWord());

        word = "future";
        wordLocation = index.search(word);
        Assert.assertEquals(word, wordLocation.getWord());

        word = "start";
        wordLocation = index.search(word);
        Assert.assertEquals(word, wordLocation.getWord());

        word = "several";
        wordLocation = index.search(word);
        Assert.assertEquals(word, wordLocation.getWord());

        word = "asdkjfaafsdkfjejafaf";
        wordLocation = index.search(word);
        Assert.assertNull(wordLocation);
    }
}
