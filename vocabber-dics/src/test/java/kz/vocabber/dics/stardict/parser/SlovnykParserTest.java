package kz.vocabber.dics.stardict.parser;

import kz.vocabber.db.model.word.Translation;
import kz.vocabber.db.model.word.Word;
import org.junit.Assert;
import org.junit.Test;

import java.util.Iterator;

/**
 * @author tabdulin
 */
public class SlovnykParserTest {
    SlovnykParser parser = new SlovnykParser();

    @Test
    public void abashed() throws Exception {
        String abashed = "<k>abashed</k>" +
                    "ABASHED" +
                    "  &quot;СМЯТЕН&quot;" +
                    "<k>abashed</k>" +
                    "ABASHED" +
                    "  &quot;СМЯТЕННЫЙ&quot;" +
                    "<k>abashed</k>" +
                    "ABASHED" +
                    "  &quot;СМУЩЕННЫЙ&quot;" +
                    "<k>abashed</k>" +
                    "ABASHED" +
                    "  &quot;СМУЩЕН&quot;" +
                    "<k>abashed</k>" +
                    "ABASHED" +
                    "  &quot;СМУЩЁННЫЙ&quot;" +
                    "<k>abashed</k>" +
                    "ABASHED" +
                    "  &quot;ОБЕСКУРАЖЕННЫЙ&quot;";


        Word word = parser.parse("abashed", abashed);

        Assert.assertTrue(word.getTranscriptions().isEmpty());
        Iterator<Translation> translationIterator = word.getTranslations().iterator();
        Assert.assertEquals("смятен", translationIterator.next().getTranslation());
        Assert.assertEquals("смятенный", translationIterator.next().getTranslation());
        Assert.assertEquals("смущенный", translationIterator.next().getTranslation());
        Assert.assertEquals("смущен", translationIterator.next().getTranslation());
        Assert.assertEquals("смущённый", translationIterator.next().getTranslation());
        Assert.assertEquals("обескураженный", translationIterator.next().getTranslation());
    }
}
