package kz.vocabber.dics.stardict.parser;

import kz.vocabber.db.model.word.Transcription;
import kz.vocabber.db.model.word.Word;
import kz.vocabber.db.model.word.Translation;
import kz.vocabber.dics.DictionaryParser;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author tabdulin
 */
public class MuellerParserTest {
    private DictionaryParser parser = new MuellerParser();

    @Test
    public void accurate() {
        String accurate = "<k>accurate</k>\n" +
                "<tr>ˈækjurɪt</tr>\n" +
                " <abr>_a.</abr>\n" +
                "<b>1&gt;</b> тОчный, прАвильный\n" +
                "<b>2&gt;</b> тщАтельный;<ex> accurate within 0</ex>,001<ex> mm </ex>с тОчностью до 0,001 мм\n" +
                "<b>3&gt;</b> мЕткий (о стрельбе)\n" +
                "<b>4&gt;</b> калибрОванный";

        Word word = parser.parse("accurate", accurate);

        Assert.assertEquals("accurate", word.getWord());

        Iterator<Transcription> transcriptionIterator = word.getTranscriptions().iterator();
        Assert.assertEquals("ˈækjurɪt", transcriptionIterator.next().getTranscription());

        Iterator<Translation> translationIterator = word.getTranslations().iterator();
        Translation translation1 = translationIterator.next();
        Assert.assertEquals("точный, правильный", translation1.getTranslation());
        Assert.assertEquals(Translation.Type.ADJECTIVE, translation1.getType());

        Translation translation2 = translationIterator.next();
        Assert.assertEquals("тщательный", translation2.getTranslation());
        Assert.assertEquals(Translation.Type.ADJECTIVE, translation2.getType());

        Translation translation3 = translationIterator.next();
        Assert.assertEquals("меткий (о стрельбе)", translation3.getTranslation());
        Assert.assertEquals(Translation.Type.ADJECTIVE, translation3.getType());

        Translation translation4 = translationIterator.next();
        Assert.assertEquals("калиброванный", translation4.getTranslation());
        Assert.assertEquals(Translation.Type.ADJECTIVE, translation4.getType());
    }

    @Test
    public void administer() {
        String administer = "<k>administer</k>\n" +
                "<tr>ədˈmɪnɪstə</tr>\n" +
                " <abr>_v.</abr>\n" +
                "<b>1&gt;</b> управлЯть; вестИ (дела)\n" +
                "<b>2&gt;</b> снабжАть; окАзывать пОмощь\n" +
                "<b>3&gt;</b> отправлЯть (правосудие); налагАть (взыскание)\n" +
                "<b>4&gt;</b><ex> to administer an oath to smb.</ex>,<ex> to administer smb. to an oath </ex>приводИть когО-л. к присЯге\n" +
                "<b>5&gt;</b> назначАть, давАть (лекарство);<ex> to administer a shock </ex>наносИть удАр";

        Word word = parser.parse("administer", administer);

        Assert.assertEquals("administer", word.getWord());

        Iterator<Transcription> transcriptionIterator = word.getTranscriptions().iterator();
        Assert.assertEquals("ədˈmɪnɪstə", transcriptionIterator.next().getTranscription());

        Iterator<Translation> translationIterator = word.getTranslations().iterator();
        Assert.assertEquals("управлять, вести (дела)", translationIterator.next().getTranslation());
        Assert.assertEquals("снабжать, оказывать помощь", translationIterator.next().getTranslation());
        Assert.assertEquals("отправлять (правосудие), налагать (взыскание)", translationIterator.next().getTranslation());
        Assert.assertEquals("назначать, давать (лекарство)", translationIterator.next().getTranslation());
    }

    @Test
    public void a1() {
        try {
            String a1 = IOUtils.toString(getClass().getResourceAsStream("/mueller/a1.xdf"));
            Word word = parser.parse("a", a1);

        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void kill() {
        try {
            String kill = IOUtils.toString(getClass().getResourceAsStream("/mueller/kill.xdf"));
            Word word = parser.parse("kill", kill);

            Assert.assertEquals(16, word.getTranslations().size());
            
            Iterator<Translation> translationIterator =  word.getTranslations().iterator();
            Assert.assertEquals("убивать, бить, резать (скот)", translationIterator.next().getTranslation());
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void fuck() {
        try {
            String fuck = IOUtils.toString(getClass().getResourceAsStream("/mueller/fuck.xdf"));
            Word word = parser.parse("fuck", fuck);

            Assert.assertEquals(1, word.getTranslations().size());

            List<Translation> translations = new ArrayList<Translation>(word.getTranslations());
            Assert.assertEquals("совокупляться", translations.get(0).getTranslation());
            Assert.assertEquals(Translation.Type.VERB, translations.get(0).getType());
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
    }
    
    public void drop() {
        try {
            String drop = IOUtils.toString(getClass().getResourceAsStream("/mueller/drop.xdf"));
            Word word = parser.parse("drop", drop);

            Assert.assertEquals(1, word.getTranslations().size());

            List<Translation> translations = new ArrayList<Translation>(word.getTranslations());
            int verbNumber = 0, nounNumber = 0;
            for (Translation translation : translations) {
                if (translation.getType() == Translation.Type.NOUN) {
                    nounNumber++;
                } else if (translation.getType() == Translation.Type.VERB) {
                    verbNumber++;
                }
            }

            Assert.assertEquals(13, nounNumber);
            Assert.assertEquals(20, verbNumber);
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
    }
}
