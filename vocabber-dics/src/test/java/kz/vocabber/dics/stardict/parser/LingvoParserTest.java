package kz.vocabber.dics.stardict.parser;

import kz.vocabber.db.model.word.Translation;
import kz.vocabber.db.model.word.Word;
import org.junit.Assert;
import org.junit.Test;

import java.util.Iterator;

/**
 * @author tabdulin
 */
public class LingvoParserTest {
    private static final String xml =
            "<k>accurate</k>\n" +
                    "<tr>ˈækjərət</tr>\n" +
                    " <abr>прил.</abr>\n" +
                    " 1) <dtrn>верный, правильный, точный</dtrn>\n" +
                    "  <ex>accurate description — точное описание</ex>\n" +
                    "  <ex>They were accurate in their prediction. — Они оказались правы в своих прогнозах.</ex>\n" +
                    "  <ex>Such a clock is guaranteed to be accurate within one second in one million years. — Точность хода таких часов находится в пределах о\\\n" +
                    "дной секунды за один миллион лет.</ex>\n" +
                    "  <b>Syn:</b>\n" +
                    "  <kref>correct</kref>, <kref>exact</kref>, <kref>precise</kref>, <kref>right</kref>, <kref>true</kref>\n" +
                    "  <b>Ant:</b>\n" +
                    "  <kref>erroneous</kref>, <kref>false</kref>, <kref>imprecise</kref>, <kref>wrong</kref>\n" +
                    " 2) <dtrn>аккуратный, скрупулёзный, тщательный</dtrn>\n" +
                    "  <ex>accurate historian — скрупулёзно относящийся к фактам историк</ex>\n" +
                    "  <ex>He is an accurate passer. — Он умеет давать точные пасы.</ex>\n" +
                    "  <b>Syn:</b>\n" +
                    "  <kref>careful</kref>, <kref>thorough</kref>\n" +
                    " 3) <dtrn><abr>воен.</abr> меткий <co>(<i>о стрельбе</i>)</co></dtrn>" +
                    "  <ex>accurate marksman — меткий стрелок</ex>\n" +
                    "  <ex>The rifle was extremely accurate. — Из этого ружья можно было очень метко стрелять.</ex>\n" +
                    "  <b>Syn:</b>\n" +
                    "  <kref>precise</kref>\n" +
                    " 4) <dtrn><abr>тех.</abr> калиброванный</dtrn>";


    @Test
    public void testParse() throws Exception {
        LingvoParser parser = new LingvoParser();
        Word word = parser.parse("accurate", xml);

        Assert.assertEquals("accurate", word.getWord());

        Assert.assertEquals("ˈækjərət", word.getTranscriptions().iterator().next().getTranscription());
        Iterator<Translation> translationIterator = word.getTranslations().iterator();
        Assert.assertEquals("верный, правильный, точный", translationIterator.next().getTranslation());
        Assert.assertEquals("аккуратный, скрупулёзный, тщательный", translationIterator.next().getTranslation());
        Assert.assertEquals("меткий", translationIterator.next().getTranslation());
        Assert.assertEquals("калиброванный", translationIterator.next().getTranslation());
    }
}
