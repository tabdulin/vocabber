package kz.vocabber.dics.stardict.parser;

import kz.vocabber.db.model.word.Translation;
import kz.vocabber.db.model.word.Word;
import org.junit.Assert;
import org.junit.Test;

import java.util.Iterator;

/**
 * @author tabdulin
 */
public class MovaParserTest {
    private String abandoned ="abandoned\n" +
            "\tбезудержный\n" +
            "\tброшенный\n" +
            "        заброшенный\n" +
            "\tнесдержанный\n" +
            "\tоставленный\n" +
            "        отказываются\n" +
            "      \tпокинутый";

    @Test
    public void testParse() throws Exception {
        MovaParser movaParser = new MovaParser();
        Word word = movaParser.parse("abandoned", abandoned);

        Assert.assertTrue(word.getTranscriptions().isEmpty());

        Iterator<Translation> translationIterator = word.getTranslations().iterator();
        Assert.assertEquals("безудержный", translationIterator.next().getTranslation());
        Assert.assertEquals("брошенный", translationIterator.next().getTranslation());
        Assert.assertEquals("заброшенный", translationIterator.next().getTranslation());
        Assert.assertEquals("несдержанный", translationIterator.next().getTranslation());
        Assert.assertEquals("оставленный", translationIterator.next().getTranslation());
        Assert.assertEquals("отказываются", translationIterator.next().getTranslation());
        Assert.assertEquals("покинутый", translationIterator.next().getTranslation());
    }
}
