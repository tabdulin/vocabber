package kz.vocabber.dics.stardict;


import junit.framework.Assert;
import org.junit.Ignore;
import org.junit.Test;

/**
 * @author tabdulin
 */
// TODO: fix
@Ignore
public class DictionaryInfoTest {

    @Test
    public void init() {
        DictionaryInfo info = new DictionaryInfo("src/test/resources/stardict/cmupd");

        Assert.assertEquals(info.getVersion(), "2.4.2");
        Assert.assertEquals(info.getWordCount(), Integer.valueOf(127012));
        Assert.assertEquals(info.getSameTypeSequence(), DictionaryInfo.SameTypeSequence.m);
    }
}
