#!/bin/sh

sudo -u postgres pg_dump --encoding=UTF8 --create --file=/tmp/vocabberdb.dump --format=custom --compress=9 --data-only vocabberdb

