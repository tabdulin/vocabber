drop database if exists vocabberdb;
drop user if exists vocabber;

create user vocabber with password 'vocabber';
create database vocabberdb with owner vocabber;
