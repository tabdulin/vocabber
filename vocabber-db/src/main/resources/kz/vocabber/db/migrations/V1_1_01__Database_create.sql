drop table if exists text_word cascade;
drop table if exists text cascade;

drop table if exists user_feedback cascade;
drop table if exists user_word cascade;
drop table if exists user_openid cascade;
drop table if exists public.user cascade;

drop table if exists word_translation cascade;
drop table if exists word_transcription cascade;
drop table if exists word cascade;

create table word (
    id bigserial constraint word_pkey primary key,
    word varchar(256) constraint word_not_null not null,

    constraint word_unique unique(word)
);

create table word_translation (
    id bigserial constraint word_translation_pkey primary key,
    word_id bigint constraint word_id_not_null not null,
    dictionary_type smallint constraint dictionary_type_not_null not null,
    translation_type smallint,
    translation text constraint translation_not_null not null,

    constraint word_id_fkey foreign key(word_id) references word,
    constraint translation_unique unique(word_id, dictionary_type, translation)
);


create table word_transcription (
    id bigserial constraint word_transcription_pkey primary key,
    word_id bigint constraint word_id_not_null not null,
    dictionary_type smallint constraint dictionary_type_not_null not null,
    transcription text constraint transcription_not_null not null,

    constraint word_id_fkey foreign key(word_id) references word,
    constraint transcription_unique unique(word_id, dictionary_type, transcription)
);

create table public.user (
    id bigserial constraint user_pkey primary key,
    nickname varchar(128) constraint nickname_not_null not null,
    first_name varchar(128),
    last_name varchar(128),
    email varchar(256),
    creation_date timestamp constraint creation_date_not_null not null,

    constraint email_unique unique(email)
);

create table user_openid (
    user_id bigint constraint user_id_not_null not null,
    openid varchar(256) constraint openid_not_null not null,
    provider varchar(256) constraint provider_not_null not null,

    constraint user_id_fkey foreign key(user_id) references public.user,
    constraint openid_unique unique(openid)
);

create table user_word (
    id bigserial constraint user_word_pkey primary  key,
    user_id bigint constraint user_id_not_null not null,
    word varchar(256) constraint word_not_null not null,
    status smallint constraint status_not_null not null,
    creation_date timestamp constraint creation_date_not_null not null,
    modification_date timestamp constraint modification_date_not_null not null,

    constraint user_id_fkey foreign key(user_id) references public.user,
    constraint user_word_unique unique(user_id, word)
);

create table user_feedback (
    id bigserial constraint user_feedback_pkey primary key,
    email varchar(256),
    content text constraint content_not_null not null,
    creation_date timestamp constraint creation_date_not_null not null
);

create table text (
    id bigserial constraint text_pkey primary key,
    user_id bigint,
    hash varchar(64) constraint hash_not_null not null,
    content text constraint content_not_null not null,
    creation_date timestamp constraint creation_date_not_null not null,

    constraint user_id_fkey foreign key(user_id) references public.user
);

create table text_word (
    id bigserial constraint text_word_pkey primary  key,
    text_id bigint constraint text_id_not_null not null,
    word varchar(256) constraint word_not_null not null,
    frequency bigint constraint word_frequency_not_null not null,

    constraint text_id_fkey foreign key(text_id) references text,
    constraint text_word_unique unique(text_id, word)
);

