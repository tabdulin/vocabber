package kz.vocabber.db.model.word;

import kz.vocabber.db.model.IdentifiedEntity;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;
import java.util.*;

/**
 * @author tabdulin
 */
@Entity
@Table(name = "word")
@FilterDef(name = Word.FILTER_DICTIONARY,
        parameters = @ParamDef(name = Word.FILTER_DICTIONARY_PARAM, type = "integer"),
        defaultCondition = "dictionary_type = :dictionary")
public class Word extends IdentifiedEntity {
    public static final String FILTER_DICTIONARY = "dictionaryFilter";
    public static final String FILTER_DICTIONARY_PARAM = "dictionary";

    private String word;

    @Filter(name = "dictionaryFilter")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "word",
            cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Transcription> transcriptions;

    @Filter(name = "dictionaryFilter")
    @OrderBy("id")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "word",
            cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Translation> translations;

    public Word() {
    }

    public Word(String word) {
        this.word = word;
        this.transcriptions = new LinkedHashSet<Transcription>(0);
        this.translations = new LinkedHashSet<Translation>(0);
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public Set<Transcription> getTranscriptions() {
        return transcriptions != null ? transcriptions : new LinkedHashSet<Transcription>();
    }

    public void setTranscriptions(Set<Transcription> transcriptions) {
        this.transcriptions = transcriptions;
    }

    public Set<Translation> getTranslations() {
        return translations != null ? translations : new LinkedHashSet<Translation>();
    }

    public void setTranslations(Set<Translation> translations) {
        this.translations = translations;
    }

    @Override
    public int hashCode() {
        return StringUtils.lowerCase(word).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Word)) {
            return false;
        }

        return StringUtils.equalsIgnoreCase(word, ((Word) obj).word);
    }

    public static class WordComparator implements Comparator<Word> {
        @Override
        public int compare(Word o1, Word o2) {
            return o1.getWord().compareTo(o2.getWord());
        }
    }
}
