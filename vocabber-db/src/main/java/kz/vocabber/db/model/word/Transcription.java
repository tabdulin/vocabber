package kz.vocabber.db.model.word;

import kz.vocabber.db.model.IdentifiedEntity;

import javax.persistence.*;

/**
 * @author tabdulin
 */
@Entity
@Table(name = "word_transcription")
public class Transcription extends IdentifiedEntity {
    @Column(name = "dictionary_type", columnDefinition = "int2")
    private Integer dictionaryType;

    @Column(nullable = false)
    private String transcription;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "word_id")
    private Word word;

    public Integer getDictionaryType() {
        return dictionaryType;
    }

    public void setDictionaryType(Integer dictionaryType) {
        this.dictionaryType = dictionaryType;
    }

    public String getTranscription() {
        return transcription;
    }

    public void setTranscription(String transcription) {
        this.transcription = transcription;
    }

    public Word getWord() {
        return word;
    }

    public void setWord(Word word) {
        this.word = word;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Transcription that = (Transcription) o;

        if (dictionaryType != null ? !dictionaryType.equals(that.dictionaryType) : that.dictionaryType != null)
            return false;
        if (transcription != null ? !transcription.equals(that.transcription) : that.transcription != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dictionaryType != null ? dictionaryType.hashCode() : 0;
        result = 31 * result + (transcription != null ? transcription.hashCode() : 0);
        return result;
    }
}
