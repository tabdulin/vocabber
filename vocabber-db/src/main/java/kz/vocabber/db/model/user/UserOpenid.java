package kz.vocabber.db.model.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

/**
 * @author tabdulin
 */
@Entity
@Table(name = "user_openid")
@PrimaryKeyJoinColumn(name = "user_id")
public class UserOpenid extends User {
    @Column(name = "openid")
    private String openid;
    @Column(name = "provider")
    private String provider;

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }
}
