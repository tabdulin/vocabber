package kz.vocabber.db.model.user;

import kz.vocabber.db.model.ModificationTimesEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Table;

/**
 * @author tabdulin
 */
@Entity
@Table(name = "user_word")
public class UserWord extends ModificationTimesEntity {
    @Column(name = "user_id")
    private Long user;

    @Column(name = "word")
    private String word;

    @Enumerated
    @Column(columnDefinition = "int2")
    private Status status;

    public static enum Status {
        learnt, forgot
    }

    public Long getUser() {
        return user;
    }

    public void setUser(Long user) {
        this.user = user;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
