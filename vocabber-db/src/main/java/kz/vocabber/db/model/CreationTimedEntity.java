package kz.vocabber.db.model;

import javax.persistence.*;
import java.util.Date;

/**
 * @author tabdulin
 */
@MappedSuperclass
public abstract class CreationTimedEntity extends IdentifiedEntity {
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creation_date")
    private Date creationDate;

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

}
