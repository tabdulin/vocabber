package kz.vocabber.db.model.word;

import kz.vocabber.db.model.IdentifiedEntity;

import javax.persistence.*;

/**
 * @author tabdulin
 */
@Entity
@Table(name = "word_translation")
public class Translation extends IdentifiedEntity {
    @Column(name = "dictionary_type", columnDefinition = "int2")
    private Integer dictionaryType;

    @Column(name = "translation_type", columnDefinition = "int2")
    @Enumerated
    private Type type;

    @Column(nullable = false)
    private String translation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "word_id")
    private Word word;

    public Integer getDictionaryType() {
        return dictionaryType;
    }

    public void setDictionaryType(Integer dictionaryType) {
        this.dictionaryType = dictionaryType;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public Word getWord() {
        return word;
    }

    public void setWord(Word word) {
        this.word = word;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Translation that = (Translation) o;

        if (dictionaryType != null ? !dictionaryType.equals(that.dictionaryType) : that.dictionaryType != null)
            return false;
        if (translation != null ? !translation.equals(that.translation) : that.translation != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = dictionaryType != null ? dictionaryType.hashCode() : 0;
        result = 31 * result + (translation != null ? translation.hashCode() : 0);
        return result;
    }

    public static enum Type {
        UNKNOWN, NOUN, VERB, ADJECTIVE, ADVERB
    }
}
