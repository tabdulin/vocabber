package kz.vocabber.db.model.user;

import kz.vocabber.db.model.CreationTimedEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author tabdulin
 */
@Entity
@Table(name = "user_feedback")
public class UserFeedback extends CreationTimedEntity {
    private String email;
    private String content;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
