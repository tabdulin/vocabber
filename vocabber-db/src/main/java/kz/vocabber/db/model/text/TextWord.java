package kz.vocabber.db.model.text;

import kz.vocabber.db.model.IdentifiedEntity;

import javax.persistence.*;

/**
 * @author tabdulin
 */
@Entity
@Table(name = "text_word")
public class TextWord extends IdentifiedEntity {
    @Column(name = "text_id")
    private Long text;

    @Column(name = "word")
    private String word;

    @Column(name = "frequency")
    private Long frequency;

    public Long getText() {
        return text;
    }

    public void setText(Long text) {
        this.text = text;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public Long getFrequency() {
        return frequency;
    }

    public void setFrequency(Long frequency) {
        this.frequency = frequency;
    }
}
