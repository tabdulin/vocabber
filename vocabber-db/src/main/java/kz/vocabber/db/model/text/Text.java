package kz.vocabber.db.model.text;

import kz.vocabber.db.model.CreationTimedEntity;

import javax.persistence.*;

/**
 * @author tabdulin
 */
@Entity
@Table(name = "text")
public class Text extends CreationTimedEntity {
    @Column(name = "user_id")
    private Long user;

    private String hash;

    private String title;

    @Enumerated
    @Column(name = "text_type", columnDefinition = "int2")
    private Type type;

    // TODO: make lazy loaded
    @Lob
    private String content;

    public Long getUser() {
        return user;
    }

    public void setUser(Long user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public static enum Type {
        FILE,
        TEXT,
        URL
    }
}
