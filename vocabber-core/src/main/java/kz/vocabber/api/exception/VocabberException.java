package kz.vocabber.api.exception;

/**
 * @author tabdulin
 */
public class VocabberException extends Exception {
    public static enum Code {
        LOGIN,
        LOGIN_NO_OPENID,

        MARK,
        WORD_NOT_REMEMBERED,
        WORD_NOT_FORGOT,

        FILE_ABSENT,
        FILE_LARGE,
        FILE_UNSUPPORTED,

        TEXT_ABSENT,
        TEXT_LARGE,

        TEXT_SAVE,

        TEXT_LOAD,
        TEXT_LOAD_SECURITY,
        TEXT_LOAD_ABSENT,

        TEXT_HISTORY_LOAD,

        LINK_ABSENT,
        LINK_DOWNLOAD,

        FEEDBACK, TEXT_HISTORY_SECURITY;
    }

    private Code code;
    private String key;
    private Object[] params;

    public VocabberException(Code code) {
        this.code = code;
    }

    public VocabberException(Code code, String key, Object... params) {
        this.code = code;
        this.key = key;
        this.params = params;
    }

    public Code getCode() {
        return code;
    }

    public String getKey() {
        return key;
    }

    public Object[] getParams() {
        return params;
    }
}
