package kz.vocabber.api.file;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author tabdulin
 */
@Service
public class FileTypeService {
    public List<FileType> getUploadable() {
        return Arrays.asList(FileType.TXT, FileType.HTML, FileType.SRT, FileType.PDF, FileType.DOC, FileType.DOCX);
    }

    public List<FileType> getDownloadable() {
        return Arrays.asList(FileType.PDF);
    }

    public FileType getByExtension(String extension) {
        for (FileType type : FileType.values()) {
            if (StringUtils.equalsIgnoreCase(extension, type.getExtension())) {
                return type;
            }
        }

        throw new IllegalArgumentException("Unsupported file extension: " + extension);
    }

    public List<String> getExtensions() {
        Set<String> extensions = new HashSet<String>();
        for (FileType type : FileType.values()) {
            extensions.add(type.getExtension());
        }

        return new ArrayList<String>(extensions);
    }

    public FileType getByMime(String mime) {
        for (FileType type : FileType.values()) {
            if (StringUtils.equalsIgnoreCase(mime, type.getMime())) {
                return type;
            }
        }

        throw new IllegalArgumentException("Unsupported file mime type: " + mime);
    }
}
