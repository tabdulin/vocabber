package kz.vocabber.api.file;

/**
 *  Mime types: http://reference.sitepoint.com/html/mime-types-full, http://filext.com/faq/office_mime_types.php
 *
 * @author tabdulin
 */
public enum FileType {
    TXT("txt", "text/plain"),
    HTML("html", "text/html"),
    SRT("srt", "application/x-subrip"),
    PDF("pdf", "application/pdf"),
    DOC("doc", "application/msword"),
    DOCX("docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");

    private String extension;
    private String mime;

    private FileType(String extension, String mime) {
        this.extension = extension;
        this.mime = mime;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }
}
