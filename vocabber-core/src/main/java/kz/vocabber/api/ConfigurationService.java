package kz.vocabber.api;

import org.springframework.stereotype.Service;

/**
 * @author tabdulin
 */
@Service
public class ConfigurationService {
    private int maximumFileSize = 5000000;
    private int wordTranslationNumber = 3;
    private int maximumTextSize = 65536; // 2^32
    private String mailSender = "vocabber@gmail.com";
    private String feedbackEmail = "vocabber@gmail.com";
    private String feedbackSubject = "[Review]";

    public int getMaximumFileSize() {
        return maximumFileSize;
    }

    public int getWordTranslationNumber() {
        return wordTranslationNumber;
    }

    public int getMaximumTextSize() {
        return maximumTextSize;
    }

    public String getMailSender() {
        return mailSender;
    }

    public String getFeedbackEmail() {
        return feedbackEmail;
    }

    public String getFeedbackSubject() {
        return feedbackSubject;
    }
}
