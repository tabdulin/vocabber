package kz.vocabber.api;

import kz.vocabber.api.dto.UserDTO;
import kz.vocabber.db.model.user.UserOpenid;

/**
 * @author tabdulin
 */
// TODO: throw UserException
public interface UserService {
    UserOpenid register(UserOpenid user);

    UserDTO getCurrentUser();

    String getUserEmail();
}

