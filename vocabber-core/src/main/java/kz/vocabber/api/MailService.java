package kz.vocabber.api;

import kz.vocabber.api.dto.MailMessageDTO;
import kz.vocabber.api.exception.MailException;

/**
 * @author tabdulin
 */
public interface MailService {
    public void send(MailMessageDTO mailMessageDTO) throws MailException;
}
