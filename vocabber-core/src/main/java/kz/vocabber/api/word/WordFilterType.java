package kz.vocabber.api.word;

/**
 * @author tabdulin
 */
public interface WordFilterType {
    public int getValue();

    public String getCode();

}
