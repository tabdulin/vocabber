package kz.vocabber.api.word;

import kz.vocabber.api.UserService;
import kz.vocabber.impl.users.UserRole;
import kz.vocabber.impl.word.filter.FileWordFilterType;
import kz.vocabber.impl.word.filter.UserWordFilterType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author tabdulin
 */
@Service
public class WordFilterTypeService {
    @Resource
    private UserService userService;
    
    public List<WordFilterType> get() {
        List<WordFilterType> filterTypes = new ArrayList<WordFilterType>();
        filterTypes.addAll(Arrays.asList(FileWordFilterType.PREPOSITIONS,
                FileWordFilterType.TOP100,
                FileWordFilterType.TOP500,
                FileWordFilterType.TOP1000_OXFORD));

        if (userService.getCurrentUser().getRole() == UserRole.ROLE_USER) {
            filterTypes.add(new UserWordFilterType());
        }

        return filterTypes;
    }

    public WordFilterType getLengthWordFilterType(final int length) {
        if (length <= 0 || length >= 10) {
            throw new IllegalArgumentException("Length word filter is not supported for such length: "
                    + length);
        }

        return new WordFilterType() {
            @Override
            public int getValue() {
                return length;
            }

            @Override
            public String getCode() {
                return "word.filter.type.length";
            }
        };
    }

    public WordFilterType get(int value) {
        for (WordFilterType type : get()) {
            if (type.getValue() == value) {
                return type;
            }
        }

        throw new IllegalArgumentException("Not support word filter type: " + value);
    }

    public WordFilterType get(String code) {
        for (WordFilterType type : get()) {
            if (StringUtils.equalsIgnoreCase(type.getCode(), code)) {
                return type;
            }
        }

        throw new IllegalArgumentException("Not support word filter type: " + code);
    }
}
