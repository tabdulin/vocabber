package kz.vocabber.api;

import kz.vocabber.api.dto.*;
import kz.vocabber.api.exception.VocabberException;
import kz.vocabber.api.word.WordFilterType;

import java.util.List;

/**
 * @author tabdulin
 */
public interface VocabberService {
    TextDTO parseFile(FileDTO fileDTO) throws VocabberException;

    TextDTO parseText(String text) throws VocabberException;

    TextDTO parseWebPage(String url) throws VocabberException;

    List<WordDTO> filter(List<WordDTO> words, List<WordFilterType> filters) throws VocabberException;

    List<WordTranslationDTO> translate(List<WordDTO> words) throws VocabberException;
    
    void mark(List<WordTranslationDTO> words) throws VocabberException;

    void remember(WordDTO... words) throws VocabberException;
    
    void forget(WordDTO word) throws VocabberException;

    void sendFeedback(FeedbackDTO feedbackDTO) throws VocabberException;

    TextDTO getText(Long textId, String hash) throws VocabberException;
    
    List<TextDTO> getHistory() throws VocabberException;
}
