package kz.vocabber.api.comparator;

import kz.vocabber.api.dto.WordDTO;

import java.util.Comparator;

/**
 * @author tabdulin
 */
public class FrequencyComparator<T extends WordDTO> implements Comparator<T> {
    private boolean ascending = true;

    public FrequencyComparator() {
    }

    public FrequencyComparator(boolean ascending) {
        this.ascending = ascending;
    }

    @Override
    public int compare(T left, T right) {
        int result = (int) (left.getFrequency() - right.getFrequency());
        if (result == 0) {
            result = left.getWord().compareTo(right.getWord());
        }

        return ascending ? result : -result;
    }
}
