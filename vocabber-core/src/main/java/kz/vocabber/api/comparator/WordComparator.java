package kz.vocabber.api.comparator;

import kz.vocabber.api.dto.WordDTO;

import java.util.Comparator;

/**
 * @author tabdulin
 */
public class WordComparator<T extends WordDTO> implements Comparator<T> {
    private boolean ascending = true;

    public WordComparator() {
    }

    public WordComparator(boolean ascending) {
        this.ascending = ascending;
    }

    @Override
    public int compare(T left, T right) {
        int result = left.getWord().compareTo(right.getWord());
        return ascending ? result : -result;
    }
}
