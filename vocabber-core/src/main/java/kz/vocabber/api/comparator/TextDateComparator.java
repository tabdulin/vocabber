package kz.vocabber.api.comparator;

import kz.vocabber.api.dto.TextDTO;

import java.util.Comparator;

/**
 * @author Talgat_Abdulin
 */
public class TextDateComparator implements Comparator<TextDTO> {
    private boolean ascending = true;

    public TextDateComparator() {
    }

    public TextDateComparator(boolean ascending) {
        this.ascending = ascending;
    }

    @Override
    public int compare(TextDTO left, TextDTO right) {
        int result = left.getDate().compareTo(right.getDate());
        return ascending ? result : -result;
    }
}
