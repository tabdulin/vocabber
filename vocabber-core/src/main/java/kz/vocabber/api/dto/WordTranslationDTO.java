package kz.vocabber.api.dto;

import java.util.Collections;
import java.util.List;

/**
 * @author tabdulin
 */
public class WordTranslationDTO extends WordDTO {
    private List<String> translations;
    private List<String> transcriptions;

    public WordTranslationDTO() {
    }

    public WordTranslationDTO(WordDTO wordDTO) {
        setId(wordDTO.getId());
        setWord(wordDTO.getWord());
        setStatus(wordDTO.getStatus());
        setFrequency(wordDTO.getFrequency());
        setTranslations(Collections.<String>emptyList());
        setTranscriptions(Collections.<String>emptyList());
    }

    public List<String> getTranslations() {
        return translations;
    }

    public void setTranslations(List<String> translations) {
        this.translations = translations;
    }

    public List<String> getTranscriptions() {
        return transcriptions;
    }

    public void setTranscriptions(List<String> transcriptions) {
        this.transcriptions = transcriptions;
    }

}
