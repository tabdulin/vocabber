package kz.vocabber.api.dto;

import kz.vocabber.impl.users.UserRole;

/**
 * @author tabdulin
 */
public class UserDTO {
    private Long id;
    private String name;
    private UserRole role;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }
}
