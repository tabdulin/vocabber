package kz.vocabber.api.dto;

import kz.vocabber.db.model.user.UserWord;

/**
 * @author tabdulin
 */
public class WordDTO {
    private Long id;
    private String word;
    private long frequency;
    private UserWord.Status status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public long getFrequency() {
        return frequency;
    }

    public void setFrequency(long frequency) {
        this.frequency = frequency;
    }

    public UserWord.Status getStatus() {
        return status;
    }

    public void setStatus(UserWord.Status status) {
        this.status = status;
    }
}
