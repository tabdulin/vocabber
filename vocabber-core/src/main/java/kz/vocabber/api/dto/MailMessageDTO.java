package kz.vocabber.api.dto;

import java.util.Arrays;
import java.util.List;

/**
 * @author tabdulin
 */
public class MailMessageDTO {
    private List<String> recipients;
    private String subject;
    private String content;
    private String replier;
    private String sender;

    public List<String> getRecipients() {
        return recipients;
    }

    public void setRecipient(String recipient) {
        this.recipients = Arrays.asList(recipient);
    }

    public void setRecipients(List<String> recipients) {
        this.recipients = recipients;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getReplier() {
        return replier;
    }

    public void setReplier(String replier) {
        this.replier = replier;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }
}
