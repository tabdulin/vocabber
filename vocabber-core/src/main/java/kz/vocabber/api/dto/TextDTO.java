package kz.vocabber.api.dto;

import kz.vocabber.db.model.text.Text;

import java.util.Date;
import java.util.List;

/**
 * @author tabdulin
 */
public class TextDTO {
    private Long id;
    private String hash;
    private String title;
    private Text.Type type;
    private Date date;
    private List<WordDTO> words;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public List<WordDTO> getWords() {
        return words;
    }

    public void setWords(List<WordDTO> words) {
        this.words = words;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Text.Type getType() {
        return type;
    }

    public void setType(Text.Type type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
