package kz.vocabber.api.dto;

import java.io.InputStream;

/**
 * @author tabdulin
 */
public class FileDTO {
    private String name;
    private String mime;
    private long size;
    private InputStream stream;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public InputStream getStream() {
        return stream;
    }

    public void setStream(InputStream stream) {
        this.stream = stream;
    }
}
