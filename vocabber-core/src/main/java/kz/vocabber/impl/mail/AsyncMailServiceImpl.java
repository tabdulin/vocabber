package kz.vocabber.impl.mail;

import kz.vocabber.api.MailService;
import kz.vocabber.api.dto.MailMessageDTO;
import kz.vocabber.api.exception.MailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author tabdulin
 */
@Service("asyncMailService")
public class AsyncMailServiceImpl implements MailService {
    private static final Logger logger = LoggerFactory.getLogger(AsyncMailServiceImpl.class);

    @Resource
    private TaskExecutor taskExecutor;
    @Resource(name = "syncMailService")
    private MailService mailService;

    @Override
    public void send(final MailMessageDTO mailMessageDTO) throws MailException {
        try {
            taskExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        mailService.send(mailMessageDTO);
                    } catch (MailException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        } catch (Exception e) {
            throw new MailException();
        }
    }
}
