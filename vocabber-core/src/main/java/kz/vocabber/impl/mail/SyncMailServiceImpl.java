package kz.vocabber.impl.mail;

import kz.vocabber.api.MailService;
import kz.vocabber.api.dto.MailMessageDTO;
import kz.vocabber.api.exception.MailException;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * @author tabdulin
 */
@Service("syncMailService")
public class SyncMailServiceImpl implements MailService {
    private static final Logger logger = LoggerFactory.getLogger(SyncMailServiceImpl.class);

    @Resource
    private JavaMailSender mailSender;

    @Override
    public void send(MailMessageDTO mailMessageDTO) throws MailException {
        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
            helper.setFrom(mailMessageDTO.getSender());
            helper.setTo(mailMessageDTO.getRecipients().toArray(new String[mailMessageDTO.getRecipients().size()]));
            if (mailMessageDTO.getReplier() != null) {
                helper.setReplyTo(mailMessageDTO.getReplier());
            }
            helper.setSubject(mailMessageDTO.getSubject());
            helper.setText(mailMessageDTO.getContent());
            mailSender.send(mimeMessage);
        } catch (MessagingException e) {
            logger.error("Mail was not sent: " + ToStringBuilder.reflectionToString(mailMessageDTO), e);
            throw new MailException();
        }
    }
}
