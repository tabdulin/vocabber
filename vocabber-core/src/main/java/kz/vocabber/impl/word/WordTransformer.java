package kz.vocabber.impl.word;

/**
 * @author Talgat_Abdulin
 */
public interface WordTransformer {
    String transform(String word);
}
