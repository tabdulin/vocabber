package kz.vocabber.impl.word.transformer;

import kz.vocabber.impl.word.WordTransformer;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;

/**
 * @author Talgat_Abdulin
 */
public class MiddleApostropheTransformer implements WordTransformer {
    public static final String APOSTROPHE = "'";
    private List<String> endings = Arrays.asList(
            "s",
            "m",
            "re",
            "d",
            "ve",
            "ll");

    @Override
    public String transform(String word) {
        if (word == null) {
            return null;
        }

        int apostrophePosition = word.indexOf(APOSTROPHE);
        if (apostrophePosition == -1
                ||  apostrophePosition == word.length() - 1) {
            return word;
        } 
        
        String ending = StringUtils.substringAfterLast(word, APOSTROPHE);
        if (!endings.contains(ending)) {
            return word;
        }

        return StringUtils.substringBeforeLast(word, APOSTROPHE);
    }
}
