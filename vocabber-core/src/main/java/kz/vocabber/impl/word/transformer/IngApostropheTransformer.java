package kz.vocabber.impl.word.transformer;

import kz.vocabber.impl.word.WordTransformer;

/**
 * @author Talgat_Abdulin
 */
public class IngApostropheTransformer implements WordTransformer {
    public static final String ING_APHOSTROPHE = "in'";
    public static final String ING = "ing";

    @Override
    public String transform(String word) {
        if (!word.endsWith(ING_APHOSTROPHE)) {
            return word;
        }

        return word.replace(ING_APHOSTROPHE, ING);
    }
}
