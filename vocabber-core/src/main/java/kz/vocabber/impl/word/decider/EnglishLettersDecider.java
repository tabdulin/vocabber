package kz.vocabber.impl.word.decider;

import kz.vocabber.impl.word.WordDecider;
import kz.vocabber.impl.word.WordExtractor;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Talgat_Abdulin
 */
public class EnglishLettersDecider implements WordDecider {

    @Override
    public boolean isWord(String token) {
        return  StringUtils.containsOnly(token, WordExtractor.ENGLISH_ALL)
                && !StringUtils.containsOnly(token, WordExtractor.ENGLISH_LETTER_DELIMITERS);
    }
}
