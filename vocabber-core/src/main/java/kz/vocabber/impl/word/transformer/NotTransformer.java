package kz.vocabber.impl.word.transformer;

import kz.vocabber.impl.word.WordTransformer;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Talgat_Abdulin
 */
public class NotTransformer implements WordTransformer {
    private static final Logger logger = LoggerFactory.getLogger(NotTransformer.class);

    @Override
    public String transform(String word) {
        if (!word.endsWith("n't")) {
            return word;
        }

        logger.trace("Transforming word: {}", word);
        String result = StringUtils.removeEnd(word, "n't");
        logger.trace("Transformation result: {}", result);

        return result;
    }
}
