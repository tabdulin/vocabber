package kz.vocabber.impl.word.filter;

import kz.vocabber.api.dto.WordDTO;
import kz.vocabber.impl.word.WordFilter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author tabdulin
 */
abstract class ExcludeWordFilter implements WordFilter {
    private static final Logger logger = LoggerFactory.getLogger(ExcludeWordFilter.class);

    @Override
    public List<WordDTO> filter(List<WordDTO> words) {
        Set<String> excludedWords = getExcludedWords();
        List<WordDTO> filtered = new ArrayList<WordDTO>(words.size());
        for (WordDTO word : words) {
            if (excludedWords.contains(StringUtils.lowerCase(word.getWord()))) {
                continue;
            }

            filtered.add(word);
        }

        logger.info("Filtered words number: {}", words.size() - filtered.size());

        return filtered;
    }

}
