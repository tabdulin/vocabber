package kz.vocabber.impl.word;

import kz.vocabber.api.word.WordFilterType;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author tabdulin
 */
@Service
public class WordFilterFactory {

    @Resource
    private ApplicationContext context;

    public List<WordFilter> get(List<WordFilterType> types) {
        List<WordFilter> chain = new ArrayList<WordFilter>(types.size());

        for (WordFilterType type : types) {
            for (WordFilter filter : context.getBeansOfType(WordFilter.class).values()) {
                if (type.getValue() == filter.getWordFilterType().getValue()) {
                    chain.add(filter);
                }
            }
        }

        return chain;
    }

}
