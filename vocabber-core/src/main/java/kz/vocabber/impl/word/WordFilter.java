package kz.vocabber.impl.word;

import kz.vocabber.api.dto.WordDTO;
import kz.vocabber.api.word.WordFilterType;

import java.util.List;
import java.util.Set;

/**
 * @author tabdulin
 */
public interface WordFilter {
    WordFilterType getWordFilterType();

    Set<String> getExcludedWords();

    List<WordDTO> filter(List<WordDTO> words);
}
