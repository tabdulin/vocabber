package kz.vocabber.impl.word;

/**
 * @author Talgat_Abdulin
 */
public interface WordDecider {
    boolean isWord(String token);
}
