package kz.vocabber.impl.word.filter;

import org.springframework.stereotype.Service;

/**
 * @author tabdulin
 */
@Service
public class Top1000OxfordWordFilter extends FileExcludeWordFilter {
    @Override
    public FileWordFilterType getWordFilterType() {
        return FileWordFilterType.TOP1000_OXFORD;
    }
}
