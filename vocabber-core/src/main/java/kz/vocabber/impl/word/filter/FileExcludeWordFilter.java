package kz.vocabber.impl.word.filter;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Set;

/**
 * @author tabdulin
 */
abstract class FileExcludeWordFilter extends ExcludeWordFilter {
    @Resource
    private FileWordFilterLoader fileWordFilter;

    private Set<String> excludedWords;

    public abstract FileWordFilterType getWordFilterType();

    @Override
    public Set<String> getExcludedWords() {
        return excludedWords;
    }

    @PostConstruct
    public void init() {
        excludedWords = fileWordFilter.getWords(getWordFilterType());
    }
}
