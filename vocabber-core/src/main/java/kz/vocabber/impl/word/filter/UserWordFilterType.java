package kz.vocabber.impl.word.filter;

import kz.vocabber.api.word.WordFilterType;

/**
 * @author Talgat_Abdulin
 */
public class UserWordFilterType implements WordFilterType {
    @Override
    public int getValue() {
        return 100;
    }

    @Override
    public String getCode() {
        return "word.filter.type.user";
    }
}
