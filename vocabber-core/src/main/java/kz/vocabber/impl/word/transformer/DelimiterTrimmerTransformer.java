package kz.vocabber.impl.word.transformer;

import kz.vocabber.impl.word.WordExtractor;
import kz.vocabber.impl.word.WordTransformer;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Talgat_Abdulin
 */
public class DelimiterTrimmerTransformer implements WordTransformer {

    @Override
    public String transform(String word) {
        if (word == null) {
            return null;
        }

        int firstLetterIndex = 0;
        for (char character : word.toCharArray()) {
            if (StringUtils.contains(WordExtractor.ENGLISH_LETTER_DELIMITERS, character)) {
                firstLetterIndex++;
            } else {
                break;
            }
        }
        
        int lastLetterIndex = word.length();
        if (firstLetterIndex < lastLetterIndex
                && !word.endsWith(IngApostropheTransformer.ING_APHOSTROPHE)) {
            for (char character : StringUtils.reverse(word).toCharArray()) {
                if (StringUtils.contains(WordExtractor.ENGLISH_LETTER_DELIMITERS, character)) {
                    lastLetterIndex--;
                } else {
                    break;
                }
            }
        }

        return word.substring(firstLetterIndex, lastLetterIndex);
    }
}
