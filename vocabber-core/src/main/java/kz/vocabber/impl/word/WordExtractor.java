package kz.vocabber.impl.word;

import kz.vocabber.api.dto.WordDTO;

import java.util.List;

/**
 * @author tabdulin
 */
public interface WordExtractor {
    final String ENGLISH_WORD_DELIMITERS = "\n\t !@#$%^&*()_+[]<>?/\\{};,.|";

    final String ENGLISH_LETTERS = "abcdefghijklmnopqrstuvwxyz";
    final String ENGLISH_LETTER_DELIMITER_HYPHEN = "-";
    final String ENGLISH_LETTER_DELIMITER_APOSTROPHE = "'";
    final String ENGLISH_LETTER_DELIMITERS = ENGLISH_LETTER_DELIMITER_APOSTROPHE + ENGLISH_LETTER_DELIMITER_HYPHEN;
    final String ENGLISH_ALL = ENGLISH_LETTERS + ENGLISH_LETTER_DELIMITERS;

    /**
         * Extracts words from text
         *
         * @param text
         * @return
         */
    List<WordDTO> extract(String text);
}
