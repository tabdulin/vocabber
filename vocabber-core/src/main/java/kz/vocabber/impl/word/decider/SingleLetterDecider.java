package kz.vocabber.impl.word.decider;

import kz.vocabber.impl.word.WordDecider;

/**
 * @author Talgat_Abdulin
 */
public class SingleLetterDecider implements WordDecider {

    @Override
    public boolean isWord(String token) {
        if (token.length() > 1) {
            return true;
        } 
        
        if (token.equalsIgnoreCase("a") 
                || token.equalsIgnoreCase("i")) {
            return true;
        }

        return false;
    }
}
