package kz.vocabber.impl.word.transformer;

import kz.vocabber.impl.word.WordTransformer;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author Talgat_Abdulin
 */
@Service
public class PluralAndPresentSimpleWordTransformer implements WordTransformer {
    private static final Logger logger = LoggerFactory.getLogger(PluralAndPresentSimpleWordTransformer.class);
    
    @Resource
    private WordsDatabase database;

    @Override
    public String transform(String word) {
        if (!word.endsWith("s") 
                && !word.endsWith("es")
                && !word.endsWith("ies")) {
            return word;
        }

        if (database.contains(word)) {
            return word;
        }

        logger.trace("Transforming word: {}", word);
        String result = word;
        if (word.endsWith("s")) {
            String single = StringUtils.removeEnd(word, "s");
            if (database.contains(single)) {
                result = single;
            }
        }

        if (word.endsWith("es")) {
            String single = StringUtils.removeEnd(word, "es");
            if (database.contains(single)) {
                result = single;
            }
        }

        if (word.endsWith("ies")) {
            String single = StringUtils.removeEnd(word, "ies") + "y";
            if (database.contains(single)) {
                result = single;
            }
        }

        logger.trace("Transformation result: {}", result);
        return result;
    }
}
