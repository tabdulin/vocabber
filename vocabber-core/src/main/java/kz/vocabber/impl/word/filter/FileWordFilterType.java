package kz.vocabber.impl.word.filter;

import kz.vocabber.api.word.WordFilterType;

/**
 * @author tabdulin
 */
public enum FileWordFilterType implements WordFilterType {
    ARTICLES(11, "word.filter.type.articles"),
    PREPOSITIONS(12, "word.filter.type.prepositions"),
    TOP100(13, "word.filter.type.top100"),
    TOP500(14, "word.filter.type.top500"),
    TOP1000_OXFORD(15, "word.filter.type.top1000.oxford");

    private int value;
    private String code;

    FileWordFilterType(int value, String code) {
        this.value = value;
        this.code = code;
    }



    @Override
    public int getValue() {
        return value;
    }

    @Override
    public String getCode() {
        return code;
    }
}
