package kz.vocabber.impl.word.filter;

import org.springframework.stereotype.Component;

/**
 * @author tabdulin
 */
@Component
public class PrepositionsWordFilter extends FileExcludeWordFilter {
    @Override
    public FileWordFilterType getWordFilterType() {
        return FileWordFilterType.PREPOSITIONS;
    }
}
