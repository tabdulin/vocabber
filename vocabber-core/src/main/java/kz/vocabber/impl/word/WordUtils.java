package kz.vocabber.impl.word;

import org.apache.commons.lang.StringUtils;

/**
 * @author tabdulin
 */
class WordUtils {
    public String trimInitialDelimeters(String word, String delimeters) {
        if (word == null) {
            return null;
        }

        int i = 0;
        for (char character : word.toCharArray()) {
            if (StringUtils.contains(delimeters, character)) {
                i++;
            } else {
                break;
            }
        }

        return i == 0 ? word : word.substring(i);
    }

}
