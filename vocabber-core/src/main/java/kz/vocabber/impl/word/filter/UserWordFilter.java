package kz.vocabber.impl.word.filter;

import kz.vocabber.api.UserService;
import kz.vocabber.api.dto.WordDTO;
import kz.vocabber.api.word.WordFilterType;
import kz.vocabber.db.model.user.UserWord;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Talgat_Abdulin
 */
@Service
public class UserWordFilter extends ExcludeWordFilter  {
    private WordFilterType type = new UserWordFilterType();
    
    @Resource
    private SessionFactory sessionFactory;
    
    @Resource
    private UserService userService;
    
    @Override
    public WordFilterType getWordFilterType() {
        return type;
    }

    @Override
    @Transactional
    public Set<String> getExcludedWords() {
        List<String> words = sessionFactory.getCurrentSession().createQuery(
                "select userWord.word " +
                "from UserWord userWord " +
                "where userWord.user = :user " +
                "    and userWord.status = :status")
            .setParameter("user", userService.getCurrentUser().getId())
            .setParameter("status", UserWord.Status.learnt)
            .list();

        return new LinkedHashSet<String>(words);
    }

    @Override
    @Transactional
    public List<WordDTO> filter(List<WordDTO> words) {
        return super.filter(words);
    }    
}
