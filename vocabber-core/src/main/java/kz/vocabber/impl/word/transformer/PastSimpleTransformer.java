package kz.vocabber.impl.word.transformer;

import kz.vocabber.impl.word.WordTransformer;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author Talgat_Abdulin
 */
@Service
public class PastSimpleTransformer implements WordTransformer {
    private static final Logger logger = LoggerFactory.getLogger(PastSimpleTransformer.class);

    @Resource
    private WordsDatabase database;
    
    @Override
    public String transform(String word) {
        if (!word.endsWith("d")
                && !word.endsWith("ed")) {
            return word;
        }
        
        if (database.contains(word)) {
            return word;
        }

        logger.trace("Transforming word: {}", word);
        String result = word;

        if (word.endsWith("d")) {
            String base = StringUtils.removeEnd(word, "d");
            if (database.contains(base)) {
                result = base;
            }
        }

        if (word.endsWith("ed")) {
            String base = StringUtils.removeEnd(word, "ed");
            if (database.contains(base)) {
                result = base;
            } else if (base.length() >= 2) {
                // fitted -> fit
                String ending = StringUtils.substring(base, base.length() - 2, base.length());
                if (ending.charAt(0) == ending.charAt(1)) {
                    String str = StringUtils.substring(base, 0, base.length() - 1);
                    if (database.contains(str)) {
                        result = str;
                    }
                }
            }
        }

        logger.trace("Transformation result: {}", result);
        return result;
    }
}
