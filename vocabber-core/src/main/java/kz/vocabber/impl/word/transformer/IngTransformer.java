package kz.vocabber.impl.word.transformer;

import kz.vocabber.impl.word.WordTransformer;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author Talgat_Abdulin
 */
@Service
public class IngTransformer implements WordTransformer {
    private static final Logger logger = LoggerFactory.getLogger(IngTransformer.class);
    
    @Resource
    private WordsDatabase database;

    @Override
    public String transform(String word) {
        if (!word.endsWith("ing")) {
            return word;
        }

        if (database.contains(word)) {
            return word;
        }
        
        logger.trace("Transforming word: {}", word);
        String result = word;

        String base = StringUtils.removeEnd(word, "ing");
        if (database.contains(base)) {
            // doing -> do
            result = base;
        } else if (database.contains(base + "e")) {
            // making -> make
            result = base + "e";
        }

        logger.trace("Transformation result: {}", result);
        return result;
    }
}
