package kz.vocabber.impl.word;

import kz.vocabber.api.dto.WordDTO;
import kz.vocabber.impl.word.decider.EnglishLettersDecider;
import kz.vocabber.impl.word.decider.SingleLetterDecider;
import kz.vocabber.impl.word.transformer.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrMatcher;
import org.apache.commons.lang3.text.StrTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author tabdulin
 */
@Service("wordExtractor")
public class WordExtractorImpl implements WordExtractor {
    private static final Logger logger = LoggerFactory.getLogger(WordExtractorImpl.class);

    @Resource
    private PluralAndPresentSimpleWordTransformer pluralAndPresentSimpleWordTransformer;
    @Resource
    private IngTransformer ingTransformer;
    @Resource
    private PastSimpleTransformer pastSimpleTransformer;

    private StrTokenizer getTokenizer(String text) {
        StrTokenizer tokenizer = new StrTokenizer(text);
        tokenizer.setDelimiterMatcher(StrMatcher.charSetMatcher(ENGLISH_WORD_DELIMITERS));
        return tokenizer;
    }

    
    /**
         * Extracts lower cased words from text and counts there frequency.
         *
         * @param text
         * @return map with words as keys and frequency as values
         */
    @Override
    public List<WordDTO> extract(String text) {
        WordDecider englishLettersDecider = new EnglishLettersDecider();
        WordDecider singleLetterDecider = new SingleLetterDecider();

        WordTransformer startingDelimitersTrimmer = new DelimiterTrimmerTransformer();
        WordTransformer middleApostropheTransformer = new MiddleApostropheTransformer();
        WordTransformer ingApostropheTransformer = new IngApostropheTransformer();
        WordTransformer notTransformer = new NotTransformer();

        Map<String,Integer> map = new HashMap<String, Integer>();
        for (String token : (List<String>) getTokenizer(text).getTokenList()) {
            String word = StringUtils.lowerCase(token);
            if (StringUtils.isBlank(word)
                    || !englishLettersDecider.isWord(word)
                    || !singleLetterDecider.isWord(word)) {
                continue;
            }

            word = startingDelimitersTrimmer.transform(word);
            word = middleApostropheTransformer.transform(word);
            word = ingApostropheTransformer.transform(word);
            word = notTransformer.transform(word);
            word = pluralAndPresentSimpleWordTransformer.transform(word);
            word = ingTransformer.transform(word);
            word = pastSimpleTransformer.transform(word);

            if (StringUtils.isEmpty(word)) {
                continue;
            }

            if (!map.containsKey(word)) {
                map.put(word, 1);
            } else {
                map.put(word, map.get(word) + 1);
            }
        }

        List<WordDTO> words = new ArrayList<WordDTO>(map.size());
        for(String word : map.keySet()) {
            WordDTO dto = new WordDTO();
            dto.setWord(word);
            dto.setFrequency(map.get(word));
            words.add(dto);
        }

        return words;
    }

}
