package kz.vocabber.impl.word.transformer;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Talgat_Abdulin
 */
@Service
class WordsDatabase {
    private static final Logger logger = LoggerFactory.getLogger(WordsDatabase.class);
    @Resource
    private SessionFactory sessionFactory;
    
    private List<String> words;

    @Transactional
    public void init() {
        try {
            logger.info("Loading all words from database");
            words = (List<String>) sessionFactory.getCurrentSession().createQuery(
                    "select distinct word " +
                    "from Word")
                    .list();
            logger.info("Words were loaded, words number: {}", words.size());
        } catch (Exception e) {
            logger.error("Words were not loaded", e);
            words = new ArrayList<String>(0);
        }
    }

    public boolean contains(String word) {
        if (words == null) {
            init();
        }

        return words.contains(word);
    }
}
