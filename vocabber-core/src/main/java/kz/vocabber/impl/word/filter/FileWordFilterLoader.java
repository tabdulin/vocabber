package kz.vocabber.impl.word.filter;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

/**
 * @author tabdulin
 */
@Service
public class FileWordFilterLoader {
    private static final Logger logger = LoggerFactory.getLogger(FileWordFilterLoader.class);

    private static final String FILTER_LOCATION = "/kz/vocabber/api/word/filter/";
    private static final String FILTER_EXTENSION = ".txt";
    private static final String FILTER_COMMENT_PREFIX = "#";


    public Set<String> getWords(FileWordFilterType type) {
        Set<String> words = new HashSet<String>();
        InputStream is = null;
        try {
            is = getClass().getResourceAsStream(FILTER_LOCATION + type.name().toLowerCase() + FILTER_EXTENSION);
            for (String line : IOUtils.readLines(is)) {
                if (StringUtils.startsWith(line, FILTER_COMMENT_PREFIX) || StringUtils.isBlank(line)) {
                    continue;
                }

                words.add(StringUtils.lowerCase(StringUtils.trim(line)));
            }
        } catch (IOException e) {
            logger.error("Filter data was not loaded for the filter: " + type, e);
        } finally {
            IOUtils.closeQuietly(is);
        }

        return words;
    }

}
