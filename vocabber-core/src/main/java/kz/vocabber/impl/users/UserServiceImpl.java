package kz.vocabber.impl.users;

import kz.vocabber.api.UserService;
import kz.vocabber.api.dto.UserDTO;
import kz.vocabber.db.model.user.UserOpenid;
import kz.vocabber.impl.users.loginza.LoginzaAuthentication;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author tabdulin
 */
@Service("userService")
public class UserServiceImpl implements UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Resource(name = "sessionFactory")
    private SessionFactory sessionFactory;

    @Transactional
    public UserOpenid register(UserOpenid user) {
        UserOpenid registeredUser = (UserOpenid) sessionFactory.getCurrentSession().createQuery(
                "from UserOpenid registeredUser " +
                "where registeredUser.openid = :openid")
                .setParameter("openid", user.getOpenid())
                .uniqueResult();

        if (registeredUser != null) {
            logger.debug("User '{}' has been already registered", registeredUser.getOpenid());
            return registeredUser;
        }

        sessionFactory.getCurrentSession().save(user);
        logger.debug("User '{}' was registered with id '{}'", user.getOpenid(), user.getId());
        return user;
    }

    @Override
    public UserDTO getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof LoginzaAuthentication) {
            VocabberUserDetails details = (VocabberUserDetails) authentication.getDetails();
            UserDTO userDTO = new UserDTO();
            userDTO.setId(details.getId());
            userDTO.setName(details.getUsername());
            userDTO.setRole(details.getRole());
            return userDTO;
        } else {
            UserDTO userDTO = new UserDTO();
            userDTO.setName("anonymous");
            userDTO.setRole(UserRole.ROLE_ANONYMOUS);
            return userDTO;
        }
    }

    @Override
    @Transactional
    public String getUserEmail() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication instanceof LoginzaAuthentication) {
            VocabberUserDetails details = (VocabberUserDetails) authentication.getDetails();
            return details.getEmail();
        } else {
            return null;
        }
    }
}
