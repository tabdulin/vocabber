package kz.vocabber.impl.users.loginza;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * @author tabdulin
 */
public class LoginzaAuthentication extends AbstractAuthenticationToken {
    private String openid;
    private String token;

    public LoginzaAuthentication(String token) {
        super(null);
        this.token = token;
    }

    public LoginzaAuthentication(String openid, String token,
                                 Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.token = token;
        this.openid = openid;
    }

    @Override
    public Object getCredentials() {
        return token;
    }

    @Override
    public Object getPrincipal() {
        return openid == null ? "loginzaUser" : openid;
    }
}
