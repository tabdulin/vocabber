package kz.vocabber.impl.users.loginza;

import kz.vocabber.api.UserService;
import kz.vocabber.api.exception.VocabberException;
import kz.vocabber.db.model.user.UserOpenid;
import kz.vocabber.impl.users.UserRole;
import kz.vocabber.impl.users.VocabberUserDetails;
import net.sf.json.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author tabdulin
 */
@Service
public class LoginzaUserDetailsService implements UserDetailsService {
    private static final Logger logger = LoggerFactory.getLogger(LoginzaUserDetailsService.class);

    @Resource
    private UserService userService;

    /*
        http://loginza.ru/json-user-profile

        Twitter: {"identity":"http:\/\/twitter.com\/vocabber","provider":"http:\/\/twitter.com\/","uid":526068212,"nickname":"vocabber","name":{"full_name":"Vocabulary Grabber"},"biography":"","photo":"http:\/\/api.twitter.com\/1\/users\/profile_image\/vocabber.json?size=bigger","web":{"default":"http:\/\/vocabber.com"}}
        MailRu: {"identity":"http:\/\/openid.mail.ru\/mail\/diggafriz","provider":"http:\/\/openid.mail.ru\/login"}
        VKontakte: {"identity":"http:\/\/vkontakte.ru\/id3723553","provider":"http:\/\/vkontakte.ru\/","uid":3723553,"name":{"first_name":"\u0422\u0430\u043b\u0433\u0430\u0442","last_name":"\u0410\u0431\u0434\u0443\u043b\u0438\u043d"},"nickname":"[talkin]","gender":"M","address":{"home":{"country":"4"}},"photo":"http:\/\/cs4396.userapi.com\/u3723553\/e_c56f7dfb.jpg"}
        Google: {"identity":"https:\/\/www.google.com\/accounts\/o8\/id?id=AItOawlOUMOQmaN6cxVFff2nkcOiywujfXcPuIA","provider":"https:\/\/www.google.com\/accounts\/o8\/ud","language":"ru","email":"vocabber@gmail.com","name":{"last_name":"Grabber","first_name":"Vocabulary","full_name":"Vocabulary Grabber"},"address":{"home":{"country":"KZ"}},"uid":"100988142274957072561"}
        Yandex: {"identity":"http:\/\/openid.yandex.ru\/kazipgo\/","provider":"http:\/\/openid.yandex.ru\/server\/"}
     */
    @Override
    public UserDetails loadUserByUsername(String auth) throws UsernameNotFoundException {
        try {
            logger.debug("Loginza auth data: {}", auth);
            JSONObject authData = JSONObject.fromObject(auth);

            UserOpenid user = new UserOpenid();
            if (!authData.has("identity")) {
                throw new VocabberException(VocabberException.Code.LOGIN_NO_OPENID);
            }

            String identity = authData.getString("identity");
            LoginzaProvider provider = getProvider(authData.getString("provider"));
            user.setCreationDate(new Date());
            user.setOpenid(identity);
            user.setProvider(provider.getId());

            if (authData.has("email")) {
                user.setEmail(authData.getString("email"));
            } else {
                switch (provider) {
                    case MAILRU:
                        user.setEmail(getIdFromIdentity(identity) + "@mail.ru");
                        break;
                    case YANDEX:
                        user.setEmail(getIdFromIdentity(identity) + "@yandex.ru");
                        break;
                }
            }

            if (authData.has("nickname")) {
                user.setNickname(authData.getString("nickname"));
            } else {
                switch (provider) {
                    case MAILRU:
                    case YANDEX:
                        user.setNickname(getIdFromIdentity(identity));
                        break;
                    case GOOGLE:
                        user.setNickname(getIdFromEmail(authData.getString("email"), "gmail.com"));
                        break;
                }
            }

            if (authData.has("name")) {
                JSONObject nameData = authData.getJSONObject("name");
                if (nameData.has("first_name")) {
                    user.setFirstName(nameData.getString("first_name"));
                }

                if (nameData.has("last_name")) {
                    user.setLastName(nameData.getString("last_name"));
                }

            }

            logger.debug("Loginza identity: {}", user.getOpenid());
            UserOpenid registeredUser = userService.register(user);
            VocabberUserDetails details = new VocabberUserDetails();
            details.setId(registeredUser.getId());
            details.setEmail(registeredUser.getEmail());
            details.setUsername(registeredUser.getNickname());
            details.setRole(UserRole.ROLE_USER);
            return details;
        } catch (Exception e) {
            logger.error("Cannot login", e);
            throw new RuntimeException(new VocabberException(VocabberException.Code.LOGIN));
        }
    }
    
    private LoginzaProvider getProvider(String provider)  {
        for (LoginzaProvider p : LoginzaProvider.values()) {
            if (StringUtils.equals(p.getId(), provider)) {
                return p;
            }
        }

        throw new IllegalArgumentException("Unsupported provider: " + provider);
    }
     
    private String getIdFromIdentity(String identity) {
        String id = StringUtils.removeEnd(identity, "/");
        id = StringUtils.substringAfterLast(id, "/");
        return id;
    }
    
    private String getIdFromEmail(String email, String domain) {
        return StringUtils.substringBefore(email, "@" + domain);
    }
}
