package kz.vocabber.impl.users;

/**
 * @author tabdulin
 */
public enum UserRole {
    ROLE_ANONYMOUS,
    ROLE_USER;
}
