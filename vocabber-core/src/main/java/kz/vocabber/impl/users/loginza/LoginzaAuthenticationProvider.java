package kz.vocabber.impl.users.loginza;

import kz.vocabber.impl.users.VocabberUserDetails;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author tabdulin
 */
@Service
public class LoginzaAuthenticationProvider implements AuthenticationProvider {
    private static final Logger logger = LoggerFactory.getLogger(LoginzaAuthenticationProvider.class);
    @Resource
    private LoginzaUserDetailsService userDetailsService;


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        try {
            logger.info("Authenticating loginza user: {}", authentication.getPrincipal());
            HttpGet get = new HttpGet("http://loginza.ru/api/authinfo?token=" + authentication.getCredentials());
            String auth = IOUtils.toString(new DefaultHttpClient().execute(get).getEntity().getContent());
            VocabberUserDetails userDetails = (VocabberUserDetails) userDetailsService.loadUserByUsername(auth);

            LoginzaAuthentication loginzaAuthentication = new LoginzaAuthentication(
                    userDetails.getUsername(),
                    (String) authentication.getCredentials(),
                    userDetails.getAuthorities());
            loginzaAuthentication.setDetails(userDetails);
            logger.info("User was authenticated: {}", loginzaAuthentication);
            return loginzaAuthentication;
        } catch (Exception e) {
            logger.error("User was not authenticated: {}", authentication);
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return LoginzaAuthentication.class.equals(authentication);
    }
}
