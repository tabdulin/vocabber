package kz.vocabber.impl.users.loginza;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author tabdulin
 */
@Service
public class LoginzaAuthenticationFilter extends AbstractAuthenticationProcessingFilter {
    private static final Logger logger = LoggerFactory.getLogger(LoginzaAuthenticationFilter.class);

    public LoginzaAuthenticationFilter() {
        super("/j_spring_security_check");
    }

    @Resource
    @Override
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        super.setAuthenticationManager(authenticationManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        String token = request.getParameter("token");
        if (!request.getMethod().equals("POST") || token == null) {
            logger.info("Not a loginza request");
            return null;
        }

        logger.trace("Token: {}", token);
        Authentication authentication = getAuthenticationManager().authenticate(new LoginzaAuthentication(token));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return authentication;
    }
}
