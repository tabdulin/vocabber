package kz.vocabber.impl.users.loginza;

/**
 * @author Talgat_Abdulin
 */
public enum LoginzaProvider {
    VKONTAKTE("http://vkontakte.ru/"),
    FACEBOOK("http://facebook.com"), // TODO: check
    GOOGLE("https://www.google.com/accounts/o8/ud"),
    YANDEX("http://openid.yandex.ru/server/"),
    MAILRU("http://openid.mail.ru/login"),
    TWITTER("http://twitter.com/");

    private String id;

    private LoginzaProvider(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
