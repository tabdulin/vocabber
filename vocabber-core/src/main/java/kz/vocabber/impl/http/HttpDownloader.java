package kz.vocabber.impl.http;


import kz.vocabber.api.exception.VocabberException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.InputStream;

/**
 * @author tabdulin
 */
@Service
public class HttpDownloader {
    private static final Logger logger = LoggerFactory.getLogger(HttpDownloader.class);

    public InputStream download(String url) throws VocabberException{
        try {
            HttpGet get = new HttpGet(url);
            HttpClient client = new DefaultHttpClient();
            HttpResponse response = client.execute(get);
            return response.getEntity().getContent();
        } catch (Exception e) {
            logger.error("Link was not downloaded", e);
            throw new VocabberException(VocabberException.Code.LINK_DOWNLOAD, "error.link.download", url);
        }
    }

}
