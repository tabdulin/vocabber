package kz.vocabber.impl.text.extractor;

import kz.vocabber.impl.text.TextExtractor;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BOMInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author tabdulin
 */
@Service("basicTextExtractor")
public class PlainTextExtractor implements TextExtractor {
    private static final Logger logger = LoggerFactory.getLogger(PlainTextExtractor.class);

    @Override
    public String extract(InputStream stream) {
        try {
            return IOUtils.toString(new BOMInputStream(stream));
        } catch (IOException e) {
            logger.error("Text was not extracted", e);
            return null;
        } finally {
            IOUtils.closeQuietly(stream);
        }
    }
}
