package kz.vocabber.impl.text.extractor;

import kz.vocabber.impl.text.TextExtractor;
import org.apache.commons.io.IOUtils;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author tabdulin
 */
@Service
public class MSWordDocTextExtractor implements TextExtractor {
    private static final Logger logger = LoggerFactory.getLogger(MSWordDocTextExtractor.class);

    @Override
    public String extract(InputStream stream) {
        try {
            WordExtractor extractor = new WordExtractor(stream);
            return extractor.getText();
        } catch (IOException e) {
            logger.error("Text was not extracted", e);
            return null;
        } finally {
            IOUtils.closeQuietly(stream);
        }
    }
}
