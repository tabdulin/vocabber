package kz.vocabber.impl.text.extractor;

import kz.vocabber.impl.text.TextExtractor;
import org.apache.commons.io.IOUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.InputStream;

/**
 * @author tabdulin
 */
@Service
public class MSWordDocxTextExtractor implements TextExtractor {
    private static final Logger logger = LoggerFactory.getLogger(MSWordDocxTextExtractor.class);

    @Override
    public String extract(InputStream stream) {
        try {
            XWPFWordExtractor extractor = new XWPFWordExtractor(OPCPackage.open(stream));
            return extractor.getText();
        } catch (Exception e) {
            logger.error("Text was not extracted", e);
            return null;
        } finally {
            IOUtils.closeQuietly(stream);
        }
    }
}
