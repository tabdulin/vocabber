package kz.vocabber.impl.text;

import kz.vocabber.api.file.FileType;
import kz.vocabber.impl.text.extractor.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author tabdulin
 */
@Service
public class TextExtractorFactory {
    @Resource
    private PlainTextExtractor plainTextExtractor;
    @Resource
    private HtmlTextExtractor htmlTextExtractor;
    @Resource
    private PdfTextExtractor pdfTextExtractor;
    @Resource
    private MSWordDocTextExtractor wordDocTextExtractor;
    @Resource
    private MSWordDocxTextExtractor wordDocxTextExtractor;

    public TextExtractor getTextExtractor(FileType type) {
        switch (type) {
            case TXT:
            case SRT:
                return plainTextExtractor;
            case HTML:
                return htmlTextExtractor;
            case PDF:
                return pdfTextExtractor;
            case DOC:
                return wordDocTextExtractor;
            case DOCX:
                return wordDocxTextExtractor;
        }

        throw new IllegalArgumentException("File type is not supported: " + type);
    };
}
