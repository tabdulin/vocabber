package kz.vocabber.impl.text.extractor;

import kz.vocabber.impl.text.TextExtractor;
import net.htmlparser.jericho.Renderer;
import net.htmlparser.jericho.Segment;
import net.htmlparser.jericho.Source;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author tabdulin
 */
@Service
public class HtmlTextExtractor implements TextExtractor{
    private static final Logger logger = LoggerFactory.getLogger(HtmlTextExtractor.class);

    @Override
    public String extract(InputStream stream) {
        try {
            String htmlText = IOUtils.toString(stream);
            Source htmlSource = new Source(htmlText);
            Segment htmlSeg = new Segment(htmlSource, 0, htmlText.length());
            Renderer htmlRend = new Renderer(htmlSeg);
            return htmlRend.toString();
        } catch (IOException e) {
            logger.error("Html was not parsed", e);
        } finally {
            IOUtils.closeQuietly(stream);
        }
        return null;
    }
}
