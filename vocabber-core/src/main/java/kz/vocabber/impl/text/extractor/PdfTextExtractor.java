package kz.vocabber.impl.text.extractor;

import kz.vocabber.impl.text.TextExtractor;
import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author tabdulin
 */
@Service("pdfTextExtractor")
public class PdfTextExtractor implements TextExtractor {
    private static final Logger logger = LoggerFactory.getLogger(PdfTextExtractor.class);

    @Override
    public String extract(InputStream stream) {
        try {
            PDDocument document = PDDocument.load(stream);
            PDFTextStripper stripper = new PDFTextStripper();
            return stripper.getText(document);
        } catch (IOException e) {
            logger.error("Pdf was not parsed", e);
        } finally {
            IOUtils.closeQuietly(stream);
        }

        return null;
    }
}
