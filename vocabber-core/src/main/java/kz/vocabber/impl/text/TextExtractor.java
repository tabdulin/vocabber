package kz.vocabber.impl.text;

import java.io.InputStream;

/**
 * @author tabdulin
 */
public interface TextExtractor {
    /**
         * Extracts text from input stream. After processing input stream is closed
         *
         * @param stream
         * @return
         */
    String extract(InputStream stream);
}
