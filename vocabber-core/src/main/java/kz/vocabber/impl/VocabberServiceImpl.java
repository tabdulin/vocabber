package kz.vocabber.impl;

import kz.vocabber.api.ConfigurationService;
import kz.vocabber.api.MailService;
import kz.vocabber.api.UserService;
import kz.vocabber.api.VocabberService;
import kz.vocabber.api.dto.*;
import kz.vocabber.api.exception.VocabberException;
import kz.vocabber.api.file.FileType;
import kz.vocabber.api.file.FileTypeService;
import kz.vocabber.api.word.WordFilterType;
import kz.vocabber.db.model.text.Text;
import kz.vocabber.db.model.text.TextWord;
import kz.vocabber.db.model.user.UserFeedback;
import kz.vocabber.db.model.user.UserWord;
import kz.vocabber.db.model.word.Transcription;
import kz.vocabber.db.model.word.Translation;
import kz.vocabber.db.model.word.Word;
import kz.vocabber.impl.http.HttpDownloader;
import kz.vocabber.impl.text.TextExtractorFactory;
import kz.vocabber.impl.word.WordExtractor;
import kz.vocabber.impl.word.WordFilter;
import kz.vocabber.impl.word.WordFilterFactory;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.FlushMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.*;

/**
 * @author tabdulin
 */
@Service("vocabberService")
public class VocabberServiceImpl implements VocabberService {
    private static final Logger logger = LoggerFactory.getLogger(VocabberServiceImpl.class);

    private static final int TRANSLATION_TYPE_NUMBER_PER_WORD = 3;

    @Resource
    private TextExtractorFactory textExtractorFactory;
    @Resource
    private WordExtractor wordExtractor;
    @Resource
    private WordFilterFactory wordFilterFactory;
    @Resource
    private FileTypeService fileTypeService;
    @Resource
    private HttpDownloader httpDownloader;
    @Resource
    private UserService userService;
    @Resource(name = "asyncMailService")
    private MailService mailService;
    @Resource
    private ConfigurationService configurationService;
    @Resource
    private SessionFactory sessionFactory;

    private TextTitleUtils textTitleUtils = new TextTitleUtils();

    @Override
    @Transactional
    public TextDTO parseFile(FileDTO fileDTO) throws VocabberException {
        logger.info("Parsing file: {} ({}) ", fileDTO.getName(), fileDTO.getMime());
        if (StringUtils.isBlank(fileDTO.getName())) {
            throw new VocabberException(VocabberException.Code.FILE_ABSENT, "error.file.absent");
        }

        if (fileDTO.getSize() > configurationService.getMaximumFileSize()) {
            throw new VocabberException(VocabberException.Code.FILE_LARGE, "error.file.large");
        }

        if (!FilenameUtils.isExtension(fileDTO.getName().toLowerCase(), fileTypeService.getExtensions())) {
            throw new VocabberException(VocabberException.Code.FILE_UNSUPPORTED, "error.file.unsupported");
        }
        
        FileType fileType = fileTypeService.getByExtension(FilenameUtils.getExtension(fileDTO.getName()));
        String text = textExtractorFactory.getTextExtractor(fileType).extract(fileDTO.getStream());
        List<WordDTO> words = wordExtractor.extract(text);
        Text savedText = save(fileDTO.getName(), Text.Type.FILE, text, words);
        logger.info("Files was parsed, words number: {}", words.size());
        return toTextDTO(savedText, words);
    }

    @Override
    @Transactional
    public TextDTO parseText(String text) throws VocabberException {
        String title = textTitleUtils.getTextTitle(text);
        logger.info("Parsing text: {}", title);
        if (StringUtils.isBlank(text)) {
            throw new VocabberException(VocabberException.Code.TEXT_ABSENT, "error.text.absent");
        }
        
        if (text.length() > configurationService.getMaximumTextSize()) {
            logger.debug("Text is too long: {}", text.length());
            throw new VocabberException(VocabberException.Code.TEXT_LARGE, "error.text.large", configurationService.getMaximumTextSize());
        }

        List<WordDTO> words = wordExtractor.extract(text);
        Text saved = save(title, Text.Type.TEXT, text, words);
        logger.info("Text was parsed, words number: {}", words.size());
        return toTextDTO(saved, words);
    }

    @Override
    @Transactional
    public TextDTO parseWebPage(String url) throws VocabberException {
        logger.info("Parsing web page: {}", url);
        if (StringUtils.isBlank(url)) {
            logger.info("Web page was not parsed: blank url");
            throw new VocabberException(VocabberException.Code.LINK_ABSENT, "error.link.absent");
        }
        
        InputStream downloadedPage = httpDownloader.download(url);
        String text = textExtractorFactory.getTextExtractor(FileType.HTML)
                .extract(downloadedPage);
        List<WordDTO> words = wordExtractor.extract(text);
        Text saved = save(url, Text.Type.URL, text, words);
        logger.info("Web page was parsed, words number: {}", words.size());
        return toTextDTO(saved, words);
    }

    @Override
    public List<WordDTO> filter(List<WordDTO> words, List<WordFilterType> filters) throws VocabberException {
        List<WordDTO> filtered = new ArrayList<WordDTO>(words);
        for (WordFilter filter : wordFilterFactory.get(filters)) {
            filtered = filter.filter(filtered);
        }

        return filtered;
    }

    private List<Word> lookup(Collection<String> words) {
        logger.debug("Looking for {} words in dictionary", words.size());
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(
                "select distinct word " +
                "from Word word " +
                "    left join fetch word.translations translation " +
                "    left join fetch word.transcriptions transcription " +
                "where word.word in (:words) ")
                .setParameterList("words", words);
        List found = query.list();
        logger.debug("Found words number: {}", found.size());
        return found;
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<WordTranslationDTO> translate(List<WordDTO> words) throws VocabberException {
        if (words.isEmpty()) {
            return new ArrayList<WordTranslationDTO>(0);
        }

        Map<String,WordDTO> map = new HashMap<String, WordDTO>(words.size());
        for (WordDTO dto : words) {
            map.put(dto.getWord(), dto);
        }

        List<WordTranslationDTO> translatedWords = new ArrayList<WordTranslationDTO>(words.size());
        for (Word word : lookup(map.keySet())) {
            WordTranslationDTO dto = new WordTranslationDTO(map.get(word.getWord()));
            dto.setId(word.getId());

            addTranslations(word, dto);

            List<String> transcriptions = new ArrayList<String>(word.getTranscriptions().size());
            for (Transcription transcription : word.getTranscriptions()) {
                transcriptions.add(transcription.getTranscription());
            }
            dto.setTranscriptions(transcriptions);

            translatedWords.add(dto);
            map.remove(word.getWord());
        }

        // untranslated words
        for (String word : map.keySet()) {
            translatedWords.add(new WordTranslationDTO(map.get(word)));
        }

        return translatedWords;
    }

    @Override
    @Transactional
    public void mark(List<WordTranslationDTO> words) throws VocabberException {
        logger.info("Marking {} words", words.size());
        if (words.isEmpty()) {
            logger.info("No words to mark");
            return;
        }

        @SuppressWarnings("unchecked")
        List<Object[]> list = sessionFactory.getCurrentSession().createQuery(
                "select word, status " +
                "from UserWord " +
                "where word in (:words) " +
                "    and user = :user")
                .setParameterList("words", getWords(words))
                .setParameter("user", userService.getCurrentUser().getId())
                .list();

        Map<String, UserWord.Status> map = new HashMap<String, UserWord.Status>(list.size());
        for (Object[] objects : list) {
            map.put((String) objects[0], (UserWord.Status) objects[1]);
        }

        for (WordTranslationDTO word : words) {
            word.setStatus(map.get(word.getWord()));
        }

        logger.info("Marked words number: {}", list.size());
    }

    private <T extends WordDTO> List<String> getWords(List<T> words) {
        List<String> list = new ArrayList<String>(words.size());
        for (T word : words) {
            list.add(word.getWord());
        }

        return list;
    }

    private void addTranslations(Word word, WordTranslationDTO dto) {
        List<String> translations = new ArrayList<String>(word.getTranslations().size());
        Map<Translation.Type, List<String>> typeTranslations =
                MapUtils.orderedMap(new HashMap<Translation.Type, List<String>>(Translation.Type.values().length));

        for (Translation translation : word.getTranslations()) {
            if (!typeTranslations.containsKey(translation.getType())) {
                typeTranslations.put(translation.getType(), new LinkedList<String>());
            }

            typeTranslations.get(translation.getType()).add(translation.getTranslation());
        }
        
        for (Map.Entry<Translation.Type, List<String>> entry : typeTranslations.entrySet()) {
            translations.addAll(entry.getValue().size() > TRANSLATION_TYPE_NUMBER_PER_WORD
                    ? entry.getValue().subList(0, 3) : entry.getValue());
        }

        dto.setTranslations(translations);
    }

    @Override
    @Transactional
    public void remember(WordDTO... words) throws VocabberException {
        try {
            logger.info("Remembering words number: {}", words.length);
            Session session = sessionFactory.getCurrentSession();
            for (WordDTO word : words) {
                logger.info("Remembering word: {}", word.getWord());
                if (word.getStatus() != null) {
                    updateWordStatus(word, UserWord.Status.learnt);
                    word.setStatus(UserWord.Status.learnt);
                    continue;
                }

                UserWord rememberedWord = new UserWord();
                Date date = new Date();
                rememberedWord.setCreationDate(date);
                rememberedWord.setModificationDate(date);
                rememberedWord.setStatus(UserWord.Status.learnt);
                rememberedWord.setUser(userService.getCurrentUser().getId());
                rememberedWord.setWord(word.getWord());
                session.save(rememberedWord);
                word.setStatus(UserWord.Status.learnt);
            }
            session.flush();
        } catch (Exception e) {
            logger.error("Words were not remembered", e);
            throw new VocabberException(VocabberException.Code.WORD_NOT_REMEMBERED, "error.word.not.remembered");
        }
    }

    @Override
    @Transactional
    public void forget(WordDTO word) throws VocabberException {
        try {
            logger.info("Forgetting word: {}", word.getWord());
            updateWordStatus(word, UserWord.Status.forgot);
            word.setStatus(UserWord.Status.forgot);
        } catch (Exception e) {
            logger.error("Word was not forgot: " + ToStringBuilder.reflectionToString(word), e);
            throw new VocabberException(VocabberException.Code.WORD_NOT_FORGOT, "error.word.not.forgot");
        }
    }

    @Override
    @Transactional
    public void sendFeedback(FeedbackDTO feedbackDTO) throws VocabberException {
        try {
            UserFeedback feedback = new UserFeedback();
            feedback.setEmail(feedbackDTO.getEmail());
            feedback.setContent(feedbackDTO.getContent());
            feedback.setCreationDate(new Date());
            sessionFactory.getCurrentSession().save(feedback);

            MailMessageDTO message = new MailMessageDTO();
            message.setSender(configurationService.getMailSender());
            message.setRecipient(configurationService.getFeedbackEmail());
            message.setSubject(configurationService.getFeedbackSubject());
            message.setReplier(feedback.getEmail());
            // TODO: templating
            message.setContent(feedbackDTO.getEmail() + "\n" + feedbackDTO.getContent());
            mailService.send(message);
        } catch (Exception e) {
            logger.error("Feedback was not saved: " + ToStringBuilder.reflectionToString(feedbackDTO), e);
            throw new VocabberException(VocabberException.Code.FEEDBACK, "feedback.error.send");
        }
    }

    @Override
    @Transactional
    public TextDTO getText(Long textId, String hash) throws VocabberException {
        try {
            logger.info("Loading text for id: {}", textId);
            Session session = sessionFactory.getCurrentSession();
            Text text = (Text) session.createQuery(
                    "from Text text " +
                    "where text.id = :id" +
                    "    and text.hash = :hash")
                    .setParameter("id", textId)
                    .setParameter("hash", hash)
                    .uniqueResult();
            if (text == null) {
                logger.info("Text was not loaded, no text with such id: {}", textId);
                throw new VocabberException(VocabberException.Code.TEXT_LOAD_ABSENT, "error.text.load.id", hash, textId);
            }

            if (text.getUser() != userService.getCurrentUser().getId()) {
                logger.info("Text was not loaded due to access restrictions. User {} trying to load text of user {}",
                        userService.getCurrentUser().getId(), text.getUser());
                throw new VocabberException(VocabberException.Code.TEXT_LOAD_SECURITY, "error.text.load.security");
            }

            List<TextWord> textWords = session.createQuery(
                    "from TextWord " +
                    "where text = :textId")
                    .setParameter("textId", textId)
                    .list();
            List<WordDTO> words = new ArrayList<WordDTO>(textWords.size());
            for (TextWord textWord : textWords) {
                WordDTO wordDTO = new WordDTO();
                wordDTO.setWord(textWord.getWord());
                wordDTO.setFrequency(textWord.getFrequency());
                words.add(wordDTO);
            }

            TextDTO textDTO = toTextDTO(text, words);
            logger.info("Text was loaded. Words number: {}", textDTO.getWords().size());
            return textDTO;
        } catch (Exception e) {
            if (e instanceof VocabberException) {
                throw (VocabberException) e;
            }

            logger.error("Text was not loaded: " + textId, e);
            throw new VocabberException(VocabberException.Code.TEXT_LOAD, "error.text.load");
        }
    }

    @Override
    @Transactional
    public List<TextDTO> getHistory() throws VocabberException {
        try {
            Long user = userService.getCurrentUser().getId();

            logger.info("Loading uploads history for user: {}", user);
            if (user == null) {
                // TODO: find out why this message is not displayed on page
                throw new VocabberException(VocabberException.Code.TEXT_HISTORY_SECURITY,
                        "error.text.history.security");
            }
            
            List<Text> texts = sessionFactory.getCurrentSession().createQuery(
                    "from Text " +
                    "where user = :user")
                    .setParameter("user", user)
                    .list();
            List<TextDTO> history = new ArrayList<TextDTO>(texts.size());
            for (Text text : texts) {
                history.add(toTextDTO(text, Collections.EMPTY_LIST));
            }
            
            logger.info("History loaded, size: {}", history.size());
            return history;
        } catch (Exception e) {
            if (e instanceof VocabberException) {
                throw (VocabberException) e;
            }

            logger.error("History was not loaded.", e);
            throw new VocabberException(VocabberException.Code.TEXT_HISTORY_LOAD, "error.text.history.load");
        }
    }

    private void updateWordStatus(WordDTO word, UserWord.Status status) throws VocabberException {
        int result = sessionFactory.getCurrentSession().createQuery(
                    "update UserWord " +
                    "set status = :status, modificationDate = :date " +
                    "where user = :user and word = :word")
                    .setParameter("date", new Date())
                    .setParameter("status", status)
                    .setParameter("user", userService.getCurrentUser().getId())
                    .setParameter("word", word.getWord())
                    .executeUpdate();

        if (result == 0) {
            throw new VocabberException(VocabberException.Code.WORD_NOT_FORGOT);
        }
    }

    private Text save(String title, Text.Type type,
                      String content, List<WordDTO> words) throws VocabberException {
        try {
            Text text = new Text();
            text.setTitle(title);
            text.setType(type);
            text.setContent(content);
            text.setCreationDate(new Date());
            text.setHash(DigestUtils.md5Hex(content));
            UserDTO user = userService.getCurrentUser();
            text.setUser(user.getId());
            Session session = sessionFactory.getCurrentSession();
            session.setFlushMode(FlushMode.MANUAL);
            session.save(text);
            int i = 0;
            for (WordDTO word : words) {
                TextWord textWord = new TextWord();
                textWord.setFrequency(word.getFrequency());
                textWord.setWord(word.getWord());
                textWord.setText(text.getId());
                session.save(textWord);
                if (i++ % 50 == 0) {
                    session.flush();
                }
            }

            session.flush();
            return text;
        } catch (Exception e) {
            logger.error("Text was not saved int database", e);
            throw new VocabberException(VocabberException.Code.TEXT_SAVE, "error.text.save");
        }
    }

    private TextDTO toTextDTO(Text saved, List<WordDTO> words) {
        TextDTO textDTO = new TextDTO();
        textDTO.setId(saved.getId());
        textDTO.setHash(saved.getHash());
        textDTO.setType(saved.getType());
        switch (saved.getType()) {
            case TEXT:
                textDTO.setTitle(saved.getTitle());
                break;
            case FILE:
                // TODO: truncate long title
                textDTO.setTitle(saved.getTitle());
                break;
            case URL:
                // TODO: truncate long title
                textDTO.setTitle(saved.getTitle());
                break;
        }
        textDTO.setDate(saved.getCreationDate());
        textDTO.setWords(words);
        return textDTO;
    }
}
