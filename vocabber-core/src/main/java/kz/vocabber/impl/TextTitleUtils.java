package kz.vocabber.impl;

import org.apache.commons.lang3.StringUtils;

/**
 * @author Talgat_Abdulin
 */
class TextTitleUtils {
    private static final int TEXT_TITLE_LENGTH = 60;
    private static final String TEXT_TITLE_ENDING = "...";
    
    public String getTextTitle(String text) {
        if (text == null) {
            return null;
        }

        String starting = StringUtils.substring(text, 0, TEXT_TITLE_LENGTH - TEXT_TITLE_ENDING.length());
        return starting.length() < TEXT_TITLE_LENGTH - TEXT_TITLE_ENDING.length()
                ? starting : starting + TEXT_TITLE_ENDING;
    }
    
    public String getFileTextTitle(String fileName) {
        throw new UnsupportedOperationException();
    }
    
    public String getUrlTextTitle(String url) {
        throw new UnsupportedOperationException();
    }
}
