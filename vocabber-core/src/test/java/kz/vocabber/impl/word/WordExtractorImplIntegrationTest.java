package kz.vocabber.impl.word;

import kz.vocabber.api.dto.WordDTO;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

/**
 * @author Talgat_Abdulin
 */
public class WordExtractorImplIntegrationTest {
    private WordExtractor wordExtractor = new WordExtractorImpl();
    
    @Test
    public void extractEmpty() throws Exception {
        Assert.assertTrue(wordExtractor.extract("").isEmpty());
    }
    
    @Test
    public void extractSingle() throws Exception {
        List<WordDTO> words = wordExtractor.extract("single");
        Assert.assertEquals(1, words.size());
        Assert.assertEquals("single", words.get(0).getWord());
    }

    @Test
    public void extractLeadingMinus() throws Exception {
        List<WordDTO> words = wordExtractor.extract("-single");
        Assert.assertEquals(1, words.size());
        Assert.assertEquals("single", words.get(0).getWord());
    }

    @Test
    public void extractLeadingApostroph() throws Exception {
        List<WordDTO> words = wordExtractor.extract("'single");
        Assert.assertEquals(1, words.size());
        Assert.assertEquals("single", words.get(0).getWord());
    }
}
