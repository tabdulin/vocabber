package kz.vocabber.impl.word.filter;

import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

/**
 * @author tabdulin
 */
public class FileWordFilterTest {
    private FileWordFilterLoader filter = new FileWordFilterLoader();

    @Test
    public void prepositions() throws Exception {
        Set<String> words = filter.getWords(FileWordFilterType.PREPOSITIONS);

        Assert.assertTrue(words.contains("abaft"));
        Assert.assertTrue(words.contains("along"));
    }

    @Test
    public void articles() throws Exception {
        Set<String> words = filter.getWords(FileWordFilterType.ARTICLES);

        Assert.assertTrue(words.contains("a"));
        Assert.assertTrue(words.contains("an"));
        Assert.assertTrue(words.contains("the"));
        Assert.assertEquals(3, words.size());
    }

    @Test
    public void top100() throws Exception {
        Set<String> words = filter.getWords(FileWordFilterType.TOP100);

        Assert.assertTrue(words.contains("a"));
        Assert.assertTrue(words.contains("at"));
        Assert.assertTrue(words.contains("the"));
        Assert.assertEquals(100, words.size());
    }

    @Test
    public void top500() throws Exception {
        Set<String> words = filter.getWords(FileWordFilterType.TOP500);

        Assert.assertEquals(499, words.size());
    }
}
