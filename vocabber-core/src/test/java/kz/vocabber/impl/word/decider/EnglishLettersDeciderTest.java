package kz.vocabber.impl.word.decider;

import kz.vocabber.impl.word.WordDecider;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Talgat_Abdulin
 */
public class EnglishLettersDeciderTest {
    @Test
    public void testIsWord() throws Exception {
        WordDecider decider = new EnglishLettersDecider();
        Assert.assertTrue(decider.isWord("a"));
        Assert.assertTrue(decider.isWord("i"));
        Assert.assertTrue(decider.isWord("alarm"));
        Assert.assertFalse(decider.isWord("русский"));
        Assert.assertFalse(decider.isWord("'"));
        Assert.assertFalse(decider.isWord("-"));

    }
}
