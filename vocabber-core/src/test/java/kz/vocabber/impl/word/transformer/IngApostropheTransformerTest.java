package kz.vocabber.impl.word.transformer;

import kz.vocabber.impl.word.WordTransformer;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Talgat_Abdulin
 */
public class IngApostropheTransformerTest {
    @Test
    public void testTransform() throws Exception {
        WordTransformer transformer = new IngApostropheTransformer();
        Assert.assertEquals(transformer.transform("doin'"), "doing");
    }
}
