package kz.vocabber.impl.word.transformer;

import kz.vocabber.impl.word.WordTransformer;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Talgat_Abdulin
 */
public class MiddleApostropheTransformerTest {
    @Test
    public void testTransform() throws Exception {
        WordTransformer transformer = new MiddleApostropheTransformer();
        Assert.assertNull(transformer.transform(null));
        Assert.assertEquals(transformer.transform(""), "");
        Assert.assertEquals(transformer.transform("i'm"), "i");
        Assert.assertEquals(transformer.transform("i'm"), "i");
        Assert.assertEquals(transformer.transform("im'"), "im'");
        Assert.assertEquals(transformer.transform("You're"), "You");
        Assert.assertEquals(transformer.transform("life's"), "life");
        Assert.assertEquals(transformer.transform("He'd"), "He");
        Assert.assertEquals(transformer.transform("'d"), "");
        Assert.assertEquals(transformer.transform("I'll"), "I");
    }
}
