package kz.vocabber.impl.word.transformer;

import kz.vocabber.impl.word.WordTransformer;
import org.junit.Assert;
import org.junit.Test;

/**
 * @author Talgat_Abdulin
 */
public class DelimiterTrimmerTransformerTest {
    private WordTransformer transformer = new DelimiterTrimmerTransformer();

    @Test
    public void testTransform() throws Exception {
        Assert.assertEquals("", transformer.transform(""));

        Assert.assertEquals("", transformer.transform("-'"));
        Assert.assertEquals("test", transformer.transform("test"));
        Assert.assertEquals("test", transformer.transform("-'test"));
        Assert.assertEquals("d", transformer.transform("'d"));
        Assert.assertEquals("we'll", transformer.transform("we'll"));

        Assert.assertEquals("we", transformer.transform("we-"));
        Assert.assertEquals("days", transformer.transform("days'"));
    }
}
