package kz.vocabber.impl;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Talgat_Abdulin
 */
public class TextTitleUtilsTest {
    private TextTitleUtils utils = new TextTitleUtils();
    
    @Test
    public void testGetTextTitle() throws Exception {
        Assert.assertNull(utils.getTextTitle(null));
        
        String source1 = "123456789 123456789 123456789 123456789 123456789 123456789 abc";
        String expected1 = "123456789 123456789 123456789 123456789 123456789 1234567...";
        Assert.assertEquals(expected1, utils.getTextTitle(source1));
        
        String source2 = "123456789";
        String expected2 = "123456789";
        Assert.assertEquals(expected2, utils.getTextTitle(source2));

        String source3 = "";
        String expected3 = "";
        Assert.assertEquals(expected3, utils.getTextTitle(source3));
    }
}
